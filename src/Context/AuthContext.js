import { createContext, useContext, useEffect, useState } from "react";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  sendEmailVerification as VerifyEmail,
  sendPasswordResetEmail as sendResetEmail,
  // reauthenticateWithCredential,
  // EmailAuthProvider,
} from "firebase/auth";
import { auth, db } from "../External Components/firebase/Firebase";
import { collection, doc, getDoc, updateDoc } from "firebase/firestore"; // Update the import statements

const UserContext = createContext();

export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState({});
  const [username, setUsername] = useState("");
  const [profileImage, setProfileImage] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [desiredMedSchool, setDesiredMedSchool] = useState("");

  const createUser = (email, password) => {
    return createUserWithEmailAndPassword(auth, email, password);
  };

  const signIn = (email, password) => {
    return signInWithEmailAndPassword(auth, email, password);
  };

  const logout = () => {
    return signOut(auth);
  };

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, async (currentUser) => {
      setUser(currentUser);
      if (currentUser) {
        const docRef = doc(db, "users", currentUser.uid); // Update this line
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
          const userData = docSnap.data();
          setUsername(userData.username);
          setProfileImage(userData.profileImage);
          setFirstName(userData.firstName);
          setLastName(userData.lastName);
          setDesiredMedSchool(userData.desiredMedSchool);
        }
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);

  const updateProfileImage = (url) => {
    setProfileImage(url);
  };

  // This is the Username update context

  const updateName = async (firstName, lastName) => {
    try {
      const userRef = doc(db, "users", user.uid);
      await updateDoc(userRef, { firstName: firstName, lastName: lastName });
      setFirstName(firstName);
      setLastName(lastName);
    } catch (error) {
      console.error(error);
    }
  };

  // This is the Medical school updating context

  const updateMedschool = async (desiredMedSchool) => {
    try {
      const userRef = doc(db, "users", user.uid);
      await updateDoc(userRef, { desiredMedSchool: desiredMedSchool });
      setDesiredMedSchool(desiredMedSchool);
    } catch (error) {
      console.error(error);
    }
  };

  // This is the Email verification logic
  const sendEmailVerification = () => {
    if (user) {
      return VerifyEmail(auth.currentUser);
    }
  };

  // This is the Password Reset Logic
  const sendPasswordResetEmail = (email) => {
    return sendResetEmail(auth, email);
  };

  //This is where the Update password logic is going to go
  // const updatePassword = async (oldPassword, newPassword) => {
  //   if (user) {
  //     const credential = EmailAuthProvider.credential(user.email, oldPassword);
  //     try {
  //       await reauthenticateWithCredential(user, credential);
  //       await updatePassword(user, newPassword);
  //       toast.success("Password updated successfully!");
  //     } catch (error) {
  //       toast.error(error.message);
  //     }
  //   }
  // };

  return (
    <UserContext.Provider
      value={{
        createUser,
        user,
        logout,
        signIn,
        username,
        profileImage,
        updateProfileImage,
        firstName,
        lastName,
        updateName,
        updateMedschool,
        desiredMedSchool,
        sendPasswordResetEmail,
        sendEmailVerification,
        // updatePassword,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const UserAuth = () => {
  return useContext(UserContext);
};
