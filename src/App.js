// Important React Dependendcies
import React, { useEffect, useState } from "react";

//Styles
import "./App.css";

// Importing the router
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

// Importing the External Components
import OnBoarding from "./External Components/OnBoarding/OnBoarding";
import Login from "./External Components/Login/Login";
import SignUp from "./External Components/SignupPage/Signup";

//Importing the Internal Components
import Dashboard from "./Internal Components/Dashboard/Dashboard";

//Creating an AuthContext to easily access the user anywhere
import { UserAuth } from "./Context/AuthContext";

//Importing the private routes
import ProtectedRoute from "./PrivateRoute";
import ErrorPage from "./External Components/ErrorPage/ErrorPage";
import SchedulePage from "./Internal Components/Schedule/SchedulePage";
import HelpandInfo from "./Internal Components/Help&Information/Help";
import Profile from "./Internal Components/Profile/Profile";

//Importing the scroll bar

function App() {
  const { user } = UserAuth();
  console.log(user);

  return (
    <>
      <Router>
        <Routes>
          {/* Use the element prop instead of the Component prop */}
          <Route path="/" element={<OnBoarding />} exact />
          <Route path="/Login" element={<Login />} exact />
          <Route path="/Signup" element={<SignUp />} exact />
          <Route path="*" element={<ErrorPage />} />

          {/* Protected Routes */}
          <Route
            path={`/Dashboard/:username`}
            element={
              user ? (
                <ProtectedRoute>
                  <Dashboard />
                </ProtectedRoute>
              ) : (
                <Navigate to="/Login" replace />
              )
            }
            exact
          />
          <Route
            path={`/Schedule/:username`}
            element={
              user ? (
                <ProtectedRoute>
                  <SchedulePage />
                </ProtectedRoute>
              ) : (
                <Navigate to="/Login" replace />
              )
            }
            exact
          />
          <Route
            path={`/HelpandInformation/:username`}
            element={
              user ? (
                <ProtectedRoute>
                  <HelpandInfo />
                </ProtectedRoute>
              ) : (
                <Navigate to="/Login" replace />
              )
            }
            exact
          />
          <Route
            path={`/Profile/:username`}
            element={
              user ? (
                <ProtectedRoute>
                  <Profile />
                </ProtectedRoute>
              ) : (
                <Navigate to="/Login" replace />
              )
            }
            exact
          />
        </Routes>
      </Router>
    </>
  );
}

export default App;
