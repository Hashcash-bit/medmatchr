// Important React Dependendcies
import React, { useState } from "react";

//Importing The Styled Components
import {
  NavbarContainer,
  NavLogo,
  NavLinks,
  LinkText,
  LoginContainer,
  LoginButton,
  SignUpButton,
  MenuToggle,
  HiddenContainer,
  TopSection,
  HiddenLinksContainer,
  HiddenLinks,
  HiddenButtonContainer,
  HiddenLoginButton,
  HiddenSignUpButton,
  CloseIcon,
} from "../OnBoarding/Components/Navbar/NavBarStyles";

// Importing The Icons
import { BiMenu } from "react-icons/bi";

// Importing Link
import { UserAuth } from "../../Context/AuthContext";

const Navbar = () => {
  // This is where the toggle menu will go
  const [toggleMenu, setToggleMenu] = useState(false);

  const handleToggleOpen = () => {
    setToggleMenu(!toggleMenu);
  };

  const handleToggleClose = () => {
    setToggleMenu(false);
  };

  const { user, username } = UserAuth();

  return (
    <>
      <NavbarContainer>
        <NavLogo>MEDMATCHR</NavLogo>
        <NavLinks></NavLinks>
        <LoginContainer></LoginContainer>
        <MenuToggle onClick={handleToggleOpen}></MenuToggle>
      </NavbarContainer>
      {toggleMenu && (
        <HiddenContainer>
          <TopSection>
            <CloseIcon onClick={handleToggleClose} />
          </TopSection>
          <HiddenLinksContainer>
            <HiddenLinks to="/">Welcome</HiddenLinks>
            <HiddenLinks to="/">Pricing</HiddenLinks>
          </HiddenLinksContainer>
          <HiddenButtonContainer>
            {user ? (
              <HiddenLoginButton to={`/Dashboard/${username}`}>
                Dashboard
              </HiddenLoginButton>
            ) : (
              <HiddenLoginButton to="/LogIn">Join Us</HiddenLoginButton>
            )}
            {!user && (
              <HiddenSignUpButton to="/Signup">SignUp</HiddenSignUpButton>
            )}
          </HiddenButtonContainer>
        </HiddenContainer>
      )}
    </>
  );
};

export default Navbar;
