//Important Rect deps
import React from "react";

//Importing the navbar
import Navbar from "./Navbar";

//Styled components
import {
  Container,
  Box,
  Title,
  Subtitle,
  Sutext,
  ButtonContainer,
  RightSection,
  Image,
  Button,
  LeftSection,
  Bottom,
  Top,
} from "./ErrorPageStyles";

//SVG
import Error404 from "./Img/ErrorSVG.svg";

//Importing User Context
import { UserAuth } from "../../Context/AuthContext";

export default function ErrorPage() {
  const { user, username } = UserAuth();

  return (
    <>
      <Container>
        <Box>
          <Top>
            <Navbar />
          </Top>
          <Bottom>
            <LeftSection>
              <Title>Error</Title>
              <Subtitle>404</Subtitle>
              <Sutext>The page does not exist!</Sutext>
              <ButtonContainer>
                {user ? (
                  <Button to={`/Dashboard/${username}`}>Dashboard</Button>
                ) : (
                  <Button to="/">Home</Button>
                )}
              </ButtonContainer>
            </LeftSection>
            <RightSection>
              <Image src={Error404} />
            </RightSection>
          </Bottom>
        </Box>
      </Container>
    </>
  );
}
