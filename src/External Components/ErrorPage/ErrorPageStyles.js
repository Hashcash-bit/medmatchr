//Importing the styled components
import { Link } from "react-router-dom";
import { styled } from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  background-color: #0f0f0f;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: default;
  font-family: Montserrat-Alt1;
`;

export const Box = styled.div`
  width: 1000px;
  display: flex;
  flex-direction: column;
  background-color: #f1eed3;
  height: 650px;
  border-radius: 25px;
  border: 2px solid #f1eed3;
  overflow: hidden;
  justify-content: space-between;

  @media screen and (max-width: 1000px) {
    height: 100%;
    border-radius: 0px;
    border: 0px;
  }
`;

export const Top = styled.div`
  height: 100px;
`;

export const Bottom = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  height: 900px;
`;

export const LeftSection = styled.div`
  height: 100%;
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media screen and (max-width: 1000px) {
    align-items: center;
    width: 100%;
  }
`;

export const Title = styled.div`
  font-size: 150px;
  font-weight: bold;
  margin-bottom: 10px;
  margin-left: 50px;

  @media screen and (max-width: 1000px) {
    margin-left: 0px;
  }

  @media screen and (max-width: 390px) {
    font-size: 120px;
  }

  @media screen and (max-width: 305px) {
    font-size: 100px;
  }
`;

export const Subtitle = styled.div`
  font-size: 100px;
  margin-bottom: 10px;
  margin-left: 50px;
  @media screen and (max-width: 1000px) {
    margin-left: 0px;
  }
`;

export const Sutext = styled.div`
  font-size: 40px;
  font-weight: bold;
  margin-bottom: 10px;
  margin-left: 50px;
  @media screen and (max-width: 1000px) {
    align-items: center;
    justify-content: center;
    display: flex;
    text-align: center;
    margin-left: 0px;
  }
`;

export const ButtonContainer = styled.div`
  width: 100%;
  margin-top: 50px;
  display: flex;

  @media screen and (max-width: 1000px) {
    align-items: center;
    justify-content: center;
  }
`;

export const Button = styled(Link)`
  padding-top: 15px;
  padding-bottom: 15px;
  padding-right: 40px;
  padding-left: 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  border-radius: 25px;
  font-weight: bold;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: 2px solid #0f0f0f;
  width: max-content;
  margin-left: 50px;
  transition: ease-in-out 0.3s;

  &:hover {
    background-color: transparent;
    color: #0f0f0f;
    border-radius: 0px;
  }

  @media screen and (max-width: 1000px) {
    margin-left: 0px;
  }
`;

export const RightSection = styled.div`
  width: 50%;
  height: 100%;
  overflow: hidden;
  justify-content: center;
  align-items: center;
  align-content: center;
  display: flex;

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

export const Image = styled.img`
  width: 100%;
  height: auto;
  justify-content: center;
  align-items: center;
  align-content: center;
`;
