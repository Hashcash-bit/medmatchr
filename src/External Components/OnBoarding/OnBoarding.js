// Importing important React Dependencies
import React, { useState } from "react";

//Importing the Navbar
import NavBar from "./Components/Navbar/NavBar";

// Importing the Hero Section
import HeroSection from "./Components/HeroSection/HeroSection";

//Importing the Pricing Section
import Pricing from "./Components/PricingSection/PricingSection";

//Importing the Footer
import Footer from "./Components/Footer/Footer";

// Importing the styled components
import { Entireelement, InternalComponents } from "./OnBoardingStyles";

//Hero Section Imports
import {
  HeroBox,
  HeroButtonContainer,
  HeroContainer,
  HeroInfo,
  HeroInfoContainer,
  HeroLeftSection,
  HeroRightSection,
  HeroSubTitle,
  HeroTitleContainer,
  HeroInfo1,
  HeroLoginButton,
  HeroSignUpButton,
  HeroImageSVG,
} from "./Components/HeroSection/HeroSectionStyle";

import MP from "./Components/HeroSection/img/MedPic.svg";

//Pricing Section Imports
import {
  PricingTitleContainer,
  PriceContainer,
  SubContainer,
  SubSubContainer,
  PlanButton,
  PlanCard,
  PlanFeatures,
  PlanName,
  PlanPrice,
  PricingSection,
  Subtitle,
  ToggleInput,
  ToggleSlider,
  ToggleSwitch,
  ToggleText,
  PlanCardContainer,
} from "./Components/PricingSection/PricingStyles";
import { Tooltip } from "./Components/PricingSection/Utils/ToolandProgress";

//Footer Section Imports
import {
  FooterContainer,
  FooterSubContainer,
  FooterLogo,
  ScrollButton,
} from "./Components/Footer/FooterStyles";
import { FaArrowUp } from "react-icons/fa";
import CourseSearch from "./Components/CourseSearch/CourseSearch";
import SearchCourse from "./Components/CourseSearch/SearchCourse";

const scrollToTop = () => {
  window.scrollTo({ top: "0", behavior: "smooth" });
};

export default function OnBoarding() {
  //Pricing toggle
  const [monthly, setMonthly] = useState(true);

  const toggleMonthly = () => {
    setMonthly(!monthly);
  };

  //Footer copy right
  const currentYear = new Date().getFullYear();

  return (
    <>
      <Entireelement>
        <InternalComponents>
          <NavBar />
          <HeroSection />
          <Pricing />
          <SearchCourse />
          <Footer />
        </InternalComponents>
      </Entireelement>
    </>
  );
}
