// Importing the styled components
import styled from "styled-components";

// Importing the Router
import { Link } from "react-router-dom";

//Importing the Icon`
import { BiX } from "react-icons/bi";

export const NavbarContainer = styled.div`
  width: 100%;
  height: 70px;
  display: flex;
  align-items: center;
  padding-top: 5px;
  gap: 40px;
  background-color: #0f0f0f;
  justify-content: space-between;
  @media screen and (max-width: 800px) {
    gap: 30px;
  }
`;

export const NavLogo = styled.div`
  font-weight: bold;
  font-size: 22px;
  color: #f1eed3;
  font-family: Montserrat-Alt1;
  align-items: center;
  align-content: center;
  align-text: center;
  display: flex;
  justify-content: center;
  margin-left: 25px;
  cursor: default;
  height: 100%;
`;

export const NavLinks = styled.div`
  height: 100%;
  justify-content: start;
  align-items: center;
  display: flex;
  width: 60%;
  gap: 40px;
  transition: ease-in-out 0.3s;

  @media screen and (max-width: 820px) {
    transition: ease-in-out 0.3s;
    justify-content: center;
  }

  @media screen and (max-width: 580px) {
    transition: ease-in-out 0.3s;
    display: none;
  }
`;

export const LinkText = styled(Link)`
  color: #c6c6c6;
  text-align: center;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  transition: transform 0.3s, box-shadow 0.3s;
  align-items: center;
  display: flex;
  // margin-bottom: 8px;

  transition: ease-in-out 0.2s;

  &:hover {
    // font-size: 18.5px;
    color: white;
    transform: translate(0, -5px);
  
  @media screen and (max-width: 685px) {
    display: none;
  }
`;

export const LoginContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  align-items: center;
  justify-content: space-around;
  width: 250px;
  margin-right: 25px;
  // margin-bottom: 5px;
  gap: 8px;

  @media screen and (max-width: 685px) {
    display: none;
  }
`;

export const LoginButton = styled(Link)`
  padding-top: 8px;
  padding-bottom: 8px;
  padding-right: 40px;
  padding-left: 40px;
  background-color: #f1eed3;
  color: #001a33;
  font-size: 18px;
  border-radius: 25px;
  font-weight: bold;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: 2px solid #f1eed3;
  width: max-content;

  &:hover {
    background-color: transparent;
    transition: ease-in-out 0.3s;
    color: #f1eed3;
    cursor: pointer;
  }
`;

export const SignUpButton = styled(Link)`
  text-decoration: none;
  font-size: 18px;
  font-weight: bold;
  color: #c6c6c6;
  transition: ease-in-out 0.2s;

  &:hover {
    color: white;
    cursor: pointer;
  }
`;

// This is where the menu toggle elements will go

export const MenuToggle = styled.div`
  display: none;
  @media screen and (max-width: 685px) {
    font-size: 30px;
    height: 70%;
    align-items: center;
    display: flex;
    margin-right: 25px;
    color: #f1eed3;
  }
`;

export const HiddenContainer = styled.div`
  display: none;
  @media screen and (max-width: 685px) {
    display: flex;
    flex-direction: column;
    height: 100vh;
    justify-content: start;
    position: fixed; /* Position the hidden menu */
    top: 0;
    left: 0;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.95); /* Semi-transparent background */
    z-index: 999; /* Set a higher z-index to place the hidden menu above other content */
  }
`;

export const TopSection = styled.div`
  display: flex;
  justify-content: end;
  height: 80px;
  align-items: center;
`;

export const CloseIcon = styled(BiX)`
  font-size: 30px;
  margin-right: 25px;
  color: #f1eed3;
  transition: ease-in-out 0.15s;
  &:hover {
    transform: translateY(-3px);
    cursor: pointer;
  }
`;

export const HiddenLinksContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 250px;
  padding-bottom: 25px;
  align-items: center;
`;

export const HiddenLinks = styled(Link)`
  color: #c6c6c6;
  text-align: center;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  transition: transform 0.3s, box-shadow 0.3s;
  align-items: center;
  display: flex;
  margin-bottom: 8px;

  transition: ease-in-out 0.2s;

  &:hover {
    font-size: 18.5px;
    color: white;
  }
`;

export const HiddenButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 150px;
  gap: 30px;
  // background-color: pink;
`;

export const HiddenLoginButton = styled(Link)`
  padding-top: 8px;
  padding-bottom: 8px;
  padding-right: 40px;
  padding-left: 40px;
  background-color: #f1eed3;
  color: #001a33;
  font-size: 18px;
  border-radius: 25px;
  font-weight: bold;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: 2px solid #f1eed3;
  width: max-content;

  &:hover {
    background-color: transparent;
    transition: ease-in-out 0.3s;
    color: #f1eed3;
    cursor: pointer;
  }
`;

export const HiddenSignUpButton = styled(Link)`
  text-decoration: none;
  font-size: 18px;
  font-weight: bold;
  color: #c6c6c6;
  transition: ease-in-out 0.2s;

  &:hover {
    color: white;
    cursor: pointer;
  }
`;
