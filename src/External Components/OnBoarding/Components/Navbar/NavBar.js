// Important React Dependendcies
import React, { useState } from "react";

//Importing The Styled Components
import {
  NavbarContainer,
  NavLogo,
  NavLinks,
  LinkText,
  LoginContainer,
  LoginButton,
  SignUpButton,
  MenuToggle,
  HiddenContainer,
  TopSection,
  HiddenLinksContainer,
  HiddenLinks,
  HiddenButtonContainer,
  HiddenLoginButton,
  HiddenSignUpButton,
  CloseIcon,
} from "./NavBarStyles";

// Importing The Icons
import { BiMenu } from "react-icons/bi";

// Importing Link
import { Link } from "react-scroll";
import { UserAuth } from "../../../../Context/AuthContext";

const NavBar = () => {
  // This is where the toggle menu will go
  const [toggleMenu, setToggleMenu] = useState(false);

  const handleToggleOpen = () => {
    setToggleMenu(!toggleMenu);
  };

  const handleToggleClose = () => {
    setToggleMenu(false);
  };

  const { user, username } = UserAuth();

  return (
    <>
      <NavbarContainer>
        <NavLogo>MEDMATCHR</NavLogo>
        <NavLinks>
          {/* <LinkText>
            <Link
              to="welcome"
              smooth={true}
              duration={500}
              // spy={true}
              // activeClass="active"
            >
              Welcome.
            </Link>
          </LinkText>
          <LinkText>
            <Link
              to="pricing"
              smooth={true}
              duration={500}
              // spy={true}
              // activeClass="active"
            >
              Pricing
            </Link>
          </LinkText>
          <LinkText>
            <Link
              to="courseChecker"
              smooth={true}
              duration={500}
              // spy={true}
              // activeClass="active"
            >
              Course Checker
            </Link>
          </LinkText> */}
        </NavLinks>
        <LoginContainer>
          {user ? (
            <LoginButton to={`/Dashboard/${username}`}>Dashboard</LoginButton>
          ) : (
            <LoginButton to="/Login">LogIn</LoginButton>
          )}
          {!user && <SignUpButton to="/Signup">SignUp</SignUpButton>}
        </LoginContainer>
        <MenuToggle onClick={handleToggleOpen}>
          <BiMenu />
        </MenuToggle>
      </NavbarContainer>
      {toggleMenu && (
        <HiddenContainer>
          <TopSection>
            <CloseIcon onClick={handleToggleClose} />
          </TopSection>
          {/* <HiddenLinksContainer>
            <HiddenLinks>
              <Link
                to="welcome"
                smooth={true}
                duration={500}
                // spy={true}
                // activeClass="active"
              >
                Welcome
              </Link>
            </HiddenLinks>
            <HiddenLinks>
              <Link
                to="pricing"
                smooth={true}
                duration={500}
                // spy={true}
                // activeClass="active"
              >
                Pricing
              </Link>
            </HiddenLinks>
            <HiddenLinks>
              <Link
                to="courseChecker"
                smooth={true}
                duration={500}
                // spy={true}
                // activeClass="active"
              >
                Course Checker
              </Link>
            </HiddenLinks>
          </HiddenLinksContainer> */}
          <HiddenButtonContainer>
            {user ? (
              <HiddenLoginButton to={`/Dashboard/${username}`}>
                Dashboard
              </HiddenLoginButton>
            ) : (
              <HiddenLoginButton to="/Login">LogIn</HiddenLoginButton>
            )}
            {!user && (
              <HiddenSignUpButton to="/Signup">SignUp</HiddenSignUpButton>
            )}
          </HiddenButtonContainer>
        </HiddenContainer>
      )}
    </>
  );
};

export default NavBar;
