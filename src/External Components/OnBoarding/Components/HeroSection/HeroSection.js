import React from "react";

//Importing the styled components
import {
  HeroBox,
  HeroButtonContainer,
  HeroContainer,
  HeroInfo,
  HeroInfoContainer,
  HeroLeftSection,
  HeroRightSection,
  HeroSubTitle,
  HeroTitleContainer,
  HeroInfo1,
  HeroLoginButton,
  HeroSignUpButton,
  HeroImageSVG,
} from "./HeroSectionStyle";

//Importing the picture
import MP from "./img/MedPic.svg";

export default function HeroSection() {
  return (
    <>
      <HeroContainer id="welcome">
        <HeroBox>
          <HeroLeftSection>
            <HeroInfoContainer>
              <div
                style={{
                  gap: "10px",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <HeroTitleContainer>
                  <HeroSubTitle>Find your future</HeroSubTitle>
                  <HeroInfo1>
                    MAXIMIZE YOUR CHANCES, UNLOCK YOUR PATH TO MEDICAL SCHOOL
                  </HeroInfo1>
                </HeroTitleContainer>
                <HeroInfo>
                  <div>
                    Optimize your course schedule and discover your eligibility
                    for medical schools with our powerful tool. Make informed
                    decisions to maximize your chances of acceptance into top
                    medical programs.
                  </div>
                </HeroInfo>
              </div>
              <HeroButtonContainer>
                <HeroLoginButton to="/Login">Welcome Back</HeroLoginButton>
                <HeroSignUpButton to="/Signup">Want to Start?</HeroSignUpButton>
              </HeroButtonContainer>
            </HeroInfoContainer>
          </HeroLeftSection>
          <HeroRightSection>
            <HeroImageSVG src={MP} />
          </HeroRightSection>
        </HeroBox>
      </HeroContainer>
    </>
  );
}
