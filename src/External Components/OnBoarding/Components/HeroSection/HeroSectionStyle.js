//Importing the styled components
import styled from "styled-components";

//Importing the Link
import { Link } from "react-router-dom";

export const HeroContainer = styled.div`
  width: 100%;
  background-color: #0f0f0f;
  height: 100%;
  display: flex;
  justify-content: center;
`;

export const HeroBox = styled.div`
  width: 96.4%;
  background-color: #0f0f0f;
  border-radius: 25px;
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 830px) {
    flex-direction: column;
    gap: 25px;
  }
`;

export const HeroLeftSection = styled.div`
  border-radius: 25px;
  width: 59%;
  height: 600px;
  background-color: #f1eed3;
  justify-content: start;

  @media screen and (max-width: 830px) {
    width: 100%;
  }
`;

export const HeroInfoContainer = styled.div`
  margin-left: 25px;
  margin-top: 25px;
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-between;
`;

export const HeroTitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 80%;
  height: max-content;
  justify-content: space-between;
  gap: 10px;

  @media screen and (max-width: 830px) {
    width: 95%;
  }
`;

export const HeroSubTitle = styled.div`
  color: #445fe2;
  font-family: Montserrat-Alt1;
  font-size: 0.9375rem;
  font-style: normal;
  font-weight: bold;
`;

export const HeroInfo1 = styled.div`
  font-family: Montserrat-Alt1;
  font-size: 65px;
  font-style: normal;
  font-weight: bold;
  color: #0f0f0f;

  @media screen and (max-width: 1273px) {
    font-size: 55px;
  }
  @media screen and (max-width: 955px) {
    font-size: 45px;
  }
  @media screen and (max-width: 830px) {
    font-size: 65px;
  }

  @media screen and (max-width: 638px) {
    font-size: 55px;
  }

  @media screen and (max-width: 500px) {
    font-size: 45px;
  }

  @media screen and (max-width: 400px) {
    font-size: 35px;
  }
`;

export const HeroInfo = styled.div`
  font-family: Montserrat-Alt1;
  font-size: 0.9375rem;
  font-style: normal;
  font-weight: 600;
  width: 75%;
  line-height: 20px;
  text-align: justify;
  text-justify: inter-word;

  @media screen and (max-width: 830px) {
    width: 90%;
  }

  @media screen and (max-width: 278px) {
    display: none;
  }
`;

export const HeroButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
  gap: 20px;
  margin-bottom: 50px;

  @media screen and (max-width: 500px) {
    flex-direction: column;
    margin-left: -25px;
  }
`;

export const HeroLoginButton = styled(Link)`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;

  &:hover {
    border-radius: 0px;
    transition: ease-in-out 0.3s;
    cursor: pointer;
  }
`;

export const HeroSignUpButton = styled(Link)`
  text-decoration: none;
  font-size: 18px;
  font-weight: bold;
  color: #0f0f0f;
  transition: ease-in-out 0.2s;
  padding: 9px 40px 10px 40px;
  border: 2px solid #0f0f0f;
  border-radius: 25px;

  &:hover {
    border-radius: 0px;
    cursor: pointer;
  }
`;

export const HeroRightSection = styled.div`
  border-radius: 25px;
  width: 39%;
  height: 600px;
  background-color: #f1eed3;
  align-items: center;
  align-content: center;
  display: flex;
  justify-content: center;

  @media screen and (max-width: 830px) {
    display: none;
  }
`;

export const HeroImageSVG = styled.img`
  width: 95%;
  height: auto;
`;
