//Importing important react dependencies
import React from "react";

//Importing Icons
import { FaArrowUp } from "react-icons/fa";

//Importing the styled components
import {
  FooterContainer,
  FooterSubContainer,
  FooterLogo,
} from "./FooterStyles";

export default function Footer() {
  //The copyright date and time
  const currentYear = new Date().getFullYear();

  return (
    <FooterContainer>
      <FooterSubContainer>
        <FooterLogo>MEDMATCHR</FooterLogo>
        <div
          style={{
            color: "#0f0f0f",
            width: "100%",
            display: "flex",
            justifyContent: "center",
          }}
        >
          &copy; {currentYear} All rights reserved.
        </div>
      </FooterSubContainer>
    </FooterContainer>
  );
}
