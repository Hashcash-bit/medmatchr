//Importing styled components
import styled from "styled-components";

//Creting the styled components
export const FooterContainer = styled.footer`
  height: max-content;
  background-color: #0f0f0f;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  color: white;
  max-width: 100%;
  padding-top: 25px;
`;

export const FooterSubContainer = styled.div`
  width: 96.4%;
  background-color: #f1eed3;
  justify-content: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: center;
  border-radius: 25px;
  padding: 10px 0;
`;

export const FooterLogo = styled.div`
  font-weight: bold;
  font-size: 22px;
  color: #0f0f0f;
  font-family: Montserrat-Alt1;
  align-items: center;
  align-content: center;
  align-text: center;
  display: flex;
  justify-content: center;
  cursor: default;
  transition: ease-in-out 0.2s;

  // Add a hover effect
  &:hover {
    transform: scale(1.1);
  }
`;

export const ScrollButton = styled.button`
  position: fixed;
  bottom: 20px;
  right: 20px;
  width: 50px;
  height: 50px;
  border: none;
  border-radius: 50%;
  background-color: #445fe2;
  color: #0f0f0f;
  font-size: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0 2 5 rgba(0, 0, 0, 0.1);
  transition: ease-in-out 0.15s;

  &:hover {
    transform: scale(1.1);
    color: white;
  }

  &:active {
    transform: scale(0.9);
  }
`;
