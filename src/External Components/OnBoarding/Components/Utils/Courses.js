let obj;

fetch('./Courses.json')
    .then((response) => response.json())
    .then(data => {
        obj = data;
    })
    .then(() => console.log(obj));

for (i = 0; i < obj.courses.length; i++) {
    let temp = [];
    temp.push(obj.courses[i].prefix)

    var uniqueCoursePrefix = [...new Set(temp)];

    console.log(uniqueCoursePrefix);
}

