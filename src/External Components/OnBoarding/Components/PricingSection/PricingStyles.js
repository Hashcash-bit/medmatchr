import { styled } from "styled-components";

//Importing the link
import { Link } from "react-router-dom";

export const PriceContainer = styled.div`
  max-width: 100%;
  background-color: #0f0f0f;
  display: flex;
  justify-content: center;
  padding-top: 25px;
`;

export const SubContainer = styled.div`
  width: 96.4%;
`;

export const SubSubContainer = styled.div`
  background-color: #f1eed3;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
  margin-top: 25px;
`;

export const Title = styled.h1`
  text-align: center;
  font-size: 36px;
  font-weight: bold;
  color: #333;
`;

export const Subtitle = styled.p`
  text-align: center;
  font-size: 18px;
  color: #0f0f0f;
  font-weight: bold;
  font-family: Montserrat-Alt1;
  padding-top: 25px;
  width: 80%;
`;

export const PricingSection = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const ToggleSwitch = styled.label`
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
`;

export const ToggleInput = styled.input`
  opacity: 0;
  width: 0;
  height: 0;

  &:checked + span {
    background-color: #0f0f0f;
  }

  &:focus + span {
    box-shadow: 0 0 1px #0f0f0f;
  }

  &:checked + span:before {
    transform: translateX(26px);
  }
`;

export const ToggleSlider = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #f1eed3;
  transition: background-color 0.4s;
  border-radius: 25px;
  border: 1px solid #0f0f0f;

  &:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: #c6c6c6;
    transition: transform 0.4s;
    border-radius: 50%;
  }
`;

export const ToggleText = styled.span`
  margin-left: 10px;
  margin-right: 10px;
  font-size: 16px;
  color: #333;
  font-weight: bold;
`;

export const PlanCard = styled.div`
  width: 300px;
  height: 400px;
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  margin: 25px 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 20px;
  background-color: #0f0f0f;

  @media screen and (max-width: 390px) {
    width: 250px;
  }

  @media screen and (max-width: 340px) {
    width: 200px;
  }
`;

export const PlanName = styled.h2`
  font-size: 24px;
  font-weight: bold;
  color: #f1eed3;
`;

export const PlanPrice = styled.p`
  font-size: 36px;
  font-weight: bold;
  color: #f1eed3;

  span {
    font-size: 18px;
    color: #666;
  }
`;

export const PlanFeatures = styled.ul`
  list-style-type: none;
  padding: 0;

  li {
    margin: 10px;
    font-size: 16px;
    color: #666;

    &::before {
      content: "✔️";
      color: #445fe2;
      margin-right: 10px;
    }
  }
`;

export const PlanButton = styled(Link)`
  width: 200px;
  height: 50px;
  border: none;
  border-radius: 25px;
  background-color: #445fe2;
  color: white;
  font-size: 18px;
  font-weight: bold;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: transform 0.3s;
  text-decoration: none;

  &:hover {
    transform: scale(1.1);
    cursor: pointer;
  }

  &:active {
    transform: scale(0.9);
  }
`;

export const TooltipContainer = styled.div`
  position: relative;

  span {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: white;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;

    &::after {
      content: "";
      position: absolute;
      top: 100%;
      left: 50%;
      margin-left: -5px;
      border-width: 5px;
      border-style: solid;
      border-color: black transparent transparent transparent;
    }
  }

  &:hover span {
    visibility: visible;
  }
`;

export const PricingTitleContainer = styled.div`
  width: 100%;
  color: #f1eed3;
  font-family: Montserrat-Alt1;
  font-size: 30px;
  font-style: normal;
  font-weight: bold;
`;

export const PlanCardContainer = styled.div`
  display: flex;
  gap: 25px;

  @media screen and (max-width: 800px) {
    flex-direction: column;
    gap: 0px;
  }
`;
