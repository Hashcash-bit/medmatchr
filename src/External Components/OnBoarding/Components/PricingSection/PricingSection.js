//Important React Dependencies
import React, { useState } from "react";

//Importing Styled Components
import {
  PricingTitleContainer,
  PriceContainer,
  SubContainer,
  SubSubContainer,
  PlanButton,
  PlanCard,
  PlanFeatures,
  PlanName,
  PlanPrice,
  PricingSection,
  Subtitle,
  ToggleInput,
  ToggleSlider,
  ToggleSwitch,
  ToggleText,
  PlanCardContainer,
} from "./PricingStyles";

//Importing The Components
import { Tooltip } from "./Utils/ToolandProgress";

const Pricing = () => {
  const [monthly, setMonthly] = useState(true);

  const toggleMonthly = () => {
    setMonthly(!monthly);
  };

  return (
    <PriceContainer id="pricing">
      <SubContainer>
        <PricingTitleContainer>Pricing</PricingTitleContainer>
        <SubSubContainer>
          <Subtitle>CHOOSE A PLAN THAT SUITS YOUR NEEDS</Subtitle>
          <PricingSection>
            <ToggleSwitch>
              <ToggleInput type="checkbox" onChange={toggleMonthly} />
              <ToggleSlider />
            </ToggleSwitch>
            <ToggleText>{monthly ? "Monthly" : "Yearly"}</ToggleText>
            <PlanCardContainer>
              <PlanCard>
                <PlanName>Basic</PlanName>
                <PlanPrice>
                  {monthly ? "$0.00" : "$0.00"}
                  <span>/ {monthly ? "month" : "year"}</span>
                </PlanPrice>
                <PlanFeatures>
                  <li>Schedule creation</li>
                  <li>Number of universities</li>
                  <li>24/7 support</li>
                </PlanFeatures>
                <PlanButton to="/Login">Select</PlanButton>
              </PlanCard>
              <PlanCard>
                <PlanName>Premium</PlanName>
                <PlanPrice>
                  {monthly ? "$5.00" : "$60.00"}
                  <span>/ {monthly ? "month" : "year"}</span>
                </PlanPrice>
                <PlanFeatures>
                  <li>What universities</li>
                  <li>Course suggestions</li>
                  <li>24/7 support</li>
                </PlanFeatures>
                <Tooltip text="Get 10% off when you upgrade">
                  <PlanButton to="/Login">Upgrade</PlanButton>
                </Tooltip>
              </PlanCard>
            </PlanCardContainer>
          </PricingSection>
        </SubSubContainer>
      </SubContainer>
    </PriceContainer>
  );
};

export default Pricing;
