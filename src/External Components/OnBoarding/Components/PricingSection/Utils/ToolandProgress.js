//Importing the styled components
import { TooltipContainer } from "../PricingStyles";
  export const Tooltip = ({ text, children }) => {
    return (
      <TooltipContainer>
        {children}
        <span>{text}</span>
      </TooltipContainer>
    );
  };