// Importing Styled Components
import { styled } from "styled-components";

export const Box = styled.div`
  background-color: #f1eed3;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
  margin-top: 25px;
`;

// define some styled components
export const Container = styled.div`
  width: 100%;
  background-color: #0f0f0f;
  height: 100%;
  display: flex;
  justify-content: center;
  padding-top: 25px;
  cursor: default;
`;

export const Form = styled.form`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding-top: 40px;
  gap: 10px;
`;

export const InputContainer = styled.div`
  display: flex;
  gap: 20px;

  @media screen and (max-width: 900px) {
    flex-direction: column;
    justify-content: center;
    align-content: center;
    align-items: center;
  }
`;

export const Info = styled.div`
  margin-top: 10px;
  font-size: 12px;
  // font-weight: bold;
  color: #b7b4a0;
  justify-content: center;
  text-align: center;
  gap: 5px;
  display: block;
  flex-direction: column;
  width: 95%;
`;

export const InfoText = styled.div`
  transition: ease-in-out 0.15s;
  width: auto;
  text-align: center;
  align-items: center;
  align-content: center;
  justify-content: center;
  display: flex;
  &:hover {
    color: #0f0f0f;
  }
`;

export const InfoH = styled.div`
  margin-top: 10px;
  font-size: 12px;
  // font-weight: bold;
  color: #b7b4a0;
  justify-content: center;
  text-align: center;
  display: block;
  flex-direction: column;
  width: 100%;
`;

export const InfoTextH = styled.div`
  transition: ease-in-out 0.15s;
  width: auto;
  text-align: center;
  align-items: center;
  align-content: center;
  justify-content: center;
  display: flex;
  margin-top: 5px;
  &:hover {
    color: #0f0f0f;
  }
`;

export const Input = styled.input`
  width: 500px;
  border-radius: 25px;
  height: 40px;
  font-size: 20px;
  padding: 10px;
  border: 0px;
  outline: 0px;
  background-color: #b7b4a0;
  color: #0f0f0f;
  font-size: 18px;

  @media screen and (max-width: 550px) {
    width: 250px;
  }
`;

export const Button = styled.button`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;

  &:hover {
    border-radius: 0px;
    border: 2px solid #0f0f0f;
    background-color: #f1eed3;
    color: #0f0f0f;
    cursor: pointer;
  }
`;

export const List = styled.ul`
  list-style: none;
  margin-bottom: 25px;
  padding: 0;
  width: 80%;
  overflow-y: auto; // make the list scrollable vertically
  max-height: 300px; // limit the height of the list

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #0f0f0f; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const Item = styled.li`
  margin: 10px;
  padding: 10px;
  border: 2px solid #0f0f0f;
  border-radius: 25px;
  background-color: #0f0f0f;
  color: #f1eed3;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media screen and (max-width: 640px) {
    flex-direction: column;
  }
`;

export const Label = styled.span`
  font-weight: bold;
`;

// The classifier popup exports

export const ResultContainer = styled.div``;

export const AddButtonContainer = styled.div`
  margin-right: 10px;
`;

export const AddButton = styled.button`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;

  &:hover {
    border-radius: 0px;
    border: 2px solid #0f0f0f;
    background-color: #f1eed3;
    color: #0f0f0f;
    cursor: pointer;
  }
`;

export const PopupContainer = styled.div`
  position: absolute;
  height: 2230px;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #0f0f0faa;
`;

export const PopupContent = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #0f0f0f;
  padding: 100px 40px;
  border-radius: 25px;
  border: 2px solid #f1eed3;
  //   min-width: 500px;
`;

export const CloseButton = styled.button`
  position: absolute;
  top: -10px;
  right: -10px;

  &:hover {
    cursor: pointer;
  }
`;

export const ClassificationContainer = styled.div`
  margin-bottom: 20px;
`;

export const ClassificationTitle = styled.div`
  font-weight: bold;
  color: #0f0f0f;
`;

export const CourseName = styled.div`
  color: #0f0f0f;
`;

export const ButtonR = styled.div`
  color: #3f3f3a;
  font-size: 12px;
  text-decoration: none;
  display: flex;
  padding-left: 40px;
  transition: ease-in-out 0.15s;

  &:hover {
    font-weight: bold;
    cursor: pointer;
  }
`;

export const ButtonVSC = styled.div`
  color: #0f0f0f;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;
  margin-bottom: 15px;
`;

export const SavecoursesContainer = styled.div`
  margin: 10px;
  padding: 10px;
  border-radius: 25px;
  color: #f1eed3;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 80%;

  @media screen and (max-width: 640px) {
    flex-direction: column;
  }
`;

export const InternalSCC = styled.div`
  list-style: none;
  margin-bottom: 25px;
  padding: 0px 32px;
  width: 100%;
  overflow-y: auto; // make the list scrollable vertically
  max-height: 300px; // limit the height of the list

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #0f0f0f; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const Select = styled.select`
  border-radius: 25px;
  height: 60px;
  width: 320px;
  font-size: 20px;
  padding: 10px;
  border: 0px;
  outline: 0px;
  background-color: #b7b4a0;
  color: #0f0f0f;
  font-size: 18px;

  @media screen and (max-width: 345px) {
    width: 250px;
  }
`;
