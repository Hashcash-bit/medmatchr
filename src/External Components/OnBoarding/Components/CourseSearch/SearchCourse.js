//Important react deps
import React, { useState, useEffect, Component } from "react";

//Course Data import
import courses from "../Utils/Course"; // import the course data

//Toast import
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//Styled Components
import styled from "styled-components";
import {
  Container,
  Box,
  Form,
  Input,
  Button,
  Info,
  InfoText,
  List,
  Item,
  InputContainer,
  AddButton,
  ClassificationContainer,
  ClassificationTitle,
  ResultContainer,
  AddButtonContainer,
  CourseName,
  ButtonR,
  ButtonVSC,
  SavecoursesContainer,
  InternalSCC,
  InfoH,
  InfoTextH,
} from "./CourseSearchStyles";

import {
  PricingTitleContainer,
  SubContainer,
} from "../PricingSection/PricingStyles";

const Select = styled.select`
  border-radius: 25px;
  height: 60px;
  width: 320px;
  font-size: 20px;
  padding: 10px;
  border: 0px;
  outline: 0px;
  background-color: #b7b4a0;
  color: #0f0f0f;
  font-size: 18px;

  @media screen and (max-width: 345px) {
    width: 250px;
  }
`;

const ResultItem = styled.div`
  margin-bottom: 5px;
  padding: 5px 10px;
`;

const Highlight = styled.span`
  background-color: #3f3f3a;
`;

const SearchCourse = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [universityFilter, setUniversityFilter] = useState("");
  const [results, setResults] = useState([]);
  const [savedCourses, setSavedCourses] = useState([]);

  useEffect(() => {
    const savedCoursesJSON = localStorage.getItem("savedCourses");
    if (savedCoursesJSON) {
      setSavedCourses(JSON.parse(savedCoursesJSON));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("savedCourses", JSON.stringify(savedCourses));
  }, [savedCourses]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!searchTerm) {
      toast.error("Please enter a search term");
      return;
    }
    let filteredCourses = courses.filter((course) => {
      if (universityFilter && course.university !== universityFilter) {
        return false;
      }
      return (
        course.university.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.prefix.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.code.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.classification.toLowerCase().includes(searchTerm.toLowerCase())
      );
    });
    setResults(filteredCourses);
  };

  const universities = [...new Set(courses.map((course) => course.university))];

  const highlightSearchTerm = (text) => {
    const parts = text.split(new RegExp(`(${searchTerm})`, "gi"));
    return (
      <>
        {parts.map((part, i) =>
          part.toLowerCase() === searchTerm.toLowerCase() ? (
            <Highlight key={i}>{part}</Highlight>
          ) : (
            part
          )
        )}
      </>
    );
  };

  const handleAddCourse = (course) => {
    setSavedCourses((prevSavedCourses) => [...prevSavedCourses, course]);
    toast.success("Course added!");
  };

  const handleRemoveCourse = (courseToRemove) => {
    setSavedCourses((prevSavedCourses) => {
      const index = prevSavedCourses.findIndex(
        (course) => course.code === courseToRemove.code
      );
      return prevSavedCourses.filter((_, i) => i !== index);
    });
  };

  const classifications = [
    ...new Set(savedCourses.map((course) => course.classification)),
  ];

  const getCoursesByClassification = (classification) =>
    savedCourses.filter((course) => course.classification === classification);

  const isCourseSaved = (course) =>
    savedCourses.some(
      (savedCourse) =>
        savedCourse.university === course.university &&
        savedCourse.prefix === course.prefix &&
        savedCourse.code === course.code
    );

  return (
    <Container id="courseChecker">
      <SubContainer>
        <PricingTitleContainer>Course Search</PricingTitleContainer>
        <Box>
          <ToastContainer />
          <Form onSubmit={handleSubmit}>
            <InputContainer>
              <Input
                type="text"
                placeholder="Enter course code, name or classification..."
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
              />
              <Select
                value={universityFilter}
                onChange={(e) => setUniversityFilter(e.target.value)}
              >
                <option value="">Search all universities</option>
                {universities.map((university) => (
                  <option key={university} value={university}>
                    {university}
                  </option>
                ))}
              </Select>
            </InputContainer>
            <Button type="submit">Submit</Button>
          </Form>
          {results.length > 0 && (
            <div>
              Number of results: <strong>{results.length}</strong>
            </div>
          )}
          <Info>
            <InfoText>
              <strong>
                Please refrain from using full text search. For example: `1aa3
                linear algebra` this will not work.
              </strong>
            </InfoText>
            <InfoText>
              <strong>
                Use keywords to search, enter your search term as such: `1aa3`,
                `MATH`, `Intermediate Spanish I`, or `Science`.
              </strong>
            </InfoText>
            <InfoText>
              <strong>
                Use whole words instead of partial words. For example: `CSC108`
                instead of `CSC1`.
              </strong>
            </InfoText>
            <InfoText>
              <strong>
                You might see alot of empy fields for the `lab` property of each
                course. This is the default standard, however the courses with
                🥼 indicate that there is a lab component associated with the
                course.
              </strong>
            </InfoText>
          </Info>
          <List>
            {results.map((result, index) => (
              <Item key={index}>
                <ResultContainer>
                  <ResultItem>
                    <strong>University:</strong>{" "}
                    {highlightSearchTerm(result.university)}
                  </ResultItem>
                  <ResultItem>
                    <strong>Prefix:</strong>{" "}
                    {highlightSearchTerm(result.prefix)}
                  </ResultItem>
                  <ResultItem>
                    <strong>Code:</strong> {highlightSearchTerm(result.code)}
                  </ResultItem>
                  <ResultItem>
                    <strong>Name:</strong> {highlightSearchTerm(result.name)}
                  </ResultItem>
                  <ResultItem>
                    <strong>Classification:</strong>{" "}
                    {highlightSearchTerm(result.classification)}
                  </ResultItem>
                  <ResultItem>
                    <strong>Lab:</strong> {highlightSearchTerm(result.lab)}
                  </ResultItem>
                </ResultContainer>
                <AddButtonContainer>
                  {!isCourseSaved(result) && (
                    <AddButton onClick={() => handleAddCourse(result)}>
                      Add
                    </AddButton>
                  )}
                </AddButtonContainer>
              </Item>
            ))}
          </List>
          {savedCourses.length > 0 && (
            <SavecoursesContainer>
              <InternalSCC>
                <>
                  <ButtonVSC>View saved courses</ButtonVSC>
                  {classifications.map((classification) => {
                    const courses = getCoursesByClassification(classification);
                    const sum = courses.reduce((acc, course) => {
                      let value;
                      const lastTwoChars = course.code.slice(-2);
                      if (lastTwoChars.match(/^\d+$/)) {
                        value = parseInt(lastTwoChars) / 3;
                      } else {
                        value = parseInt(course.code.slice(-1)) / 3;
                      }
                      value = Math.round(value);
                      return acc + value;
                    }, 0);
                    return (
                      <ClassificationContainer key={classification}>
                        <ClassificationTitle>
                          {classification} ({sum})
                        </ClassificationTitle>
                        {courses.map((course, index) => (
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "space-between",
                            }}
                            key={course.code}
                          >
                            <CourseName>{course.name} </CourseName>
                            <ButtonR onClick={() => handleRemoveCourse(course)}>
                              Remove
                            </ButtonR>
                          </div>
                        ))}
                      </ClassificationContainer>
                    );
                  })}
                </>
              </InternalSCC>
              <InfoH>
                <InfoTextH>
                  <div
                    style={{
                      color: "#f1eed3",
                      backgroundColor: "#0f0f0f",
                      padding: "20px",
                      borderRadius: "25px",
                    }}
                  >
                    <strong>Disclaimers:</strong>
                    {""} Please keep in mind each individual university has
                    their own specific requirements for certain courses, so
                    while one course may meet the prerequisite for one
                    professional school, it may not for another (for example
                    `PSYCH` may count as Humanities for one school but count as
                    Science for another)
                  </div>
                </InfoTextH>
                <InfoTextH>
                  For this counter, 3 units count as a one semester course, and
                  6 units count as two etc.
                </InfoTextH>
                <InfoTextH>
                  Also keep in mind that `ARTSSCI`, `ISCI`, `IARTS` courses are
                  not counted towards any prerequisites here even though some
                  courses may count for certain universities
                </InfoTextH>
                <InfoTextH>
                  Lastly, all physics and math related courses under the
                  engineering department are not counted as prerequisites due to
                  the logical assumption that in order to take them, first year
                  engineering math and physics courses should have already been
                  taken, which meets the course prerequisites for most medical
                  schools already.
                </InfoTextH>
              </InfoH>
            </SavecoursesContainer>
          )}
        </Box>
      </SubContainer>
    </Container>
  );
};

export default SearchCourse;
