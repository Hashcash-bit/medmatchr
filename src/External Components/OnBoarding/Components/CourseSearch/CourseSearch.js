import React, { useState, useEffect } from "react";
import styled from "styled-components";
import courses from "../Utils/Course"; // import the course data

import { toast, ToastContainer } from "react-toastify"; // import the toast function
import "react-toastify/dist/ReactToastify.css"; // import the toastify css

import Highlighter from "react-highlight-words"; // import the Highlighter component
import {
  Box,
  Button,
  Container,
  Form,
  Info,
  InfoText,
  Input,
  Item,
  Label,
  List,
} from "./CourseSearchStyles";

//Styled Components
import {
  PricingTitleContainer,
  SubContainer,
} from "../PricingSection/PricingStyles";

// define the CourseSearch component
const CourseSearch = () => {
  const [search, setSearch] = useState(""); // state for the search input
  const [results, setResults] = useState([]); // state for the filtered results

  // handle the submit event of the form element
  const handleSubmit = (e) => {
    // prevent the default behavior of reloading the page
    e.preventDefault();
    // check if the input is empty
    if (search.trim() === "") {
      // if the input is empty, display an error message using toast
      toast.error("Please enter a valid search term", {
        position: toast.POSITION.TOP_CENTER,
      });
    } else {
      // if the input is not empty, filter the courses by code, name or classification
      const filteredCourses = courses.filter(
        (course) =>
          course.prefix.toLowerCase().includes(search.toLowerCase()) ||
          course.code.toLowerCase().includes(search.toLowerCase()) ||
          course.name.toLowerCase().includes(search.toLowerCase()) ||
          course.classification.toLowerCase().includes(search.toLowerCase())
      );
      // update the results state with the filtered courses
      setResults(filteredCourses);
      // clear the search input
      setSearch("");
      // check if the filteredCourses array is empty
      if (filteredCourses.length === 0) {
        // if the filteredCourses array is empty, display an info message using toast
        toast.info(
          "No course by your search requirements has been found, please try again.",
          {
            position: toast.POSITION.TOP_CENTER,
          }
        );
      }
    }
  };

  // handle the change event of the input element
  const handleChange = (e) => {
    // update the search state with the input value
    setSearch(e.target.value);
  };

  return (
    <Container>
      <SubContainer>
        <PricingTitleContainer>Course Search</PricingTitleContainer>
        <Box>
          <ToastContainer />
          <Form onSubmit={handleSubmit}>
            <Input
              type="text"
              value={search}
              onChange={handleChange}
              placeholder="Enter course code, name or classification"
            />
            <Button type="submit">Search</Button>
          </Form>
          <Info>
            <InfoText></InfoText>
          </Info>
          <p>{results.length} courses found</p>{" "}
          {/* display the number of results */}
          <List>
            {results.map((course) => (
              <Item key={course.code}>
                <p>
                  <Label>Course Prefix:</Label>{" "}
                  {/* use the Highlighter component to highlight the searched term */}
                  <Highlighter
                    highlightClassName="highlighted" // specify a class name for highlighted words
                    searchWords={[search]} // pass the search state as a prop
                    autoEscape={true} // escape special characters
                    textToHighlight={course.prefix} // pass the text to highlight
                  />
                </p>
                <p>
                  <Label>Course Code:</Label>{" "}
                  {/* use the Highlighter component to highlight the searched term */}
                  <Highlighter
                    highlightClassName="highlighted" // specify a class name for highlighted words
                    searchWords={[search]} // pass the search state as a prop
                    autoEscape={true} // escape special characters
                    textToHighlight={course.code} // pass the text to highlight
                  />
                </p>
                <p>
                  <Label>Course Name:</Label>{" "}
                  {/* use the Highlighter component to highlight the searched term */}
                  <Highlighter
                    highlightClassName="highlighted" // specify a class name for highlighted words
                    searchWords={[search]} // pass the search state as a prop
                    autoEscape={true} // escape special characters
                    textToHighlight={course.name} // pass the text to highlight
                  />
                </p>
                <p>
                  <Label>Classification:</Label>{" "}
                  {/* use the Highlighter component to highlight the searched term */}
                  <Highlighter
                    highlightClassName="highlighted" // specify a class name for highlighted words
                    searchWords={[search]} // pass the search state as a prop
                    autoEscape={true} // escape special characters
                    textToHighlight={course.classification} // pass the text to highlight
                  />
                </p>
              </Item>
            ))}
          </List>
        </Box>
      </SubContainer>
    </Container>
  );
};

export default CourseSearch;
