//Import the react dependencies
import React, { useState } from "react";

// Styled components for the login page
import {
  AuthorBio,
  AuthorContainer,
  AuthorPic,
  Box,
  ButtonContainer,
  Container,
  DHAA,
  Form,
  InformationContainer,
  Intro,
  JobTitle,
  LeftBox,
  LeftSection,
  LoginButton,
  LogoLink,
  Name,
  Quote,
  QuoteBox,
  QuoteContainer,
  RightSection,
  SignupContainer,
  SignupLink,
  SubIntro,
  Title,
  TitleContainer,
  AuthorPicContainer,
  RightBox,
  EmailContainer,
  EmailText,
  PasswordContainer,
  PasswordText,
  PInputContainer,
} from "./SignupStyles";

//Importing the image
import AR from "./img/AR.jpg";

//Importing the icons
import { RiEyeLine, RiEyeOffLine } from "react-icons/ri";

//Importing the Routing
import { useNavigate } from "react-router-dom";

//Importing the Toasts
import { ToastContainer, toast } from "react-toastify"; // Import react-toastify components
import "react-toastify/dist/ReactToastify.css"; // Import react-toastify CSS file

//Importing the firebase dependencies
import { auth, db } from "../firebase/Firebase";
import { collection, doc, setDoc, addDoc } from "firebase/firestore";

// Using Context
import { UserAuth } from "../../Context/AuthContext";

// The main component for the login and sign up page
export default function SignUp() {
  // State variables for the username, email, password, confirm password
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const history = useNavigate();
  const navigate = useNavigate();

  //The user context
  const { createUser, username, sendEmailVerification } = UserAuth();

  //Toggle the password view
  const [showPassword, setShowPassword] = useState(false);
  const [showRePassword, setShowRePassword] = useState(false);

  const handleToggle = () => {
    setShowPassword(!showPassword);
  };

  const handleToggleRe = () => {
    setShowRePassword(!showRePassword);
  };

  // Some regex error catching const method (pretty cool)--
  const lowercaseRegex = /[a-z]/; //                      |
  const uppercaseRegex = /[A-Z]/; //                      |
  const numberRegex = /[0-9]/; //                         |
  // ------------------------------------------------------

  //Time to do some error handling
  const [showErrors, setShowErrors] = useState(false);
  const [errors, setErrors] = useState({});

  const getErrors = (userName, email, password, confirmPassword) => {
    const errors = {};

    // This is the error handling for the username
    if (!userName) {
      errors.userName = "Please enter a username";
    }

    // This is the error handling for the email
    if (!email) {
      errors.email = "Please enter your email";
    } else if (
      typeof email === "string" &&
      (!email.includes("@") || !email.includes(".com"))
    ) {
      errors.email = "Please enter a proper email (johndoe@whatever.com)";
    }

    if (!password) {
      errors.password = "Please enter your password";
    } else if (typeof password === "string" && password.length < 8) {
      errors.password = "The password must be at least 8 characters long";
    } else if (typeof password === "string" && !/[!@#$%^&]/.test(password)) {
      errors.password = "Password must include any of these (!,@,#,$,%,^,&)";
    } else if (typeof password === "string" && password === email) {
      errors.password =
        "Please make sure that your email and password are not the same";
    } else if (typeof password === "string" && !lowercaseRegex.test(password)) {
      errors.password = "Please include at least 1 lowercase letter";
    } else if (typeof password === "string" && !uppercaseRegex.test(password)) {
      errors.password = "Please include at least 1 capital letter";
    } else if (typeof password === "string" && !numberRegex.test(password)) {
      errors.password = "Please include at least 1 number";
    }

    if (confirmPassword !== password) {
      errors.password = "Please make sure that your passwords match";
    }

    return errors;
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    const errors = getErrors(userName, email, password, confirmPassword);

    if (Object.keys(errors).length > 0) {
      setShowErrors(true);
      setErrors(errors);

      Object.values(errors).forEach((errorMsg) => {
        toast.error(errorMsg);
      });
    } else {
      try {
        await createUser(email, password);
        navigate(`/Dashboard/${userName}`);
        toast.success("You have successfully created an account!");

        // Save the username under the user's uid in Firestore
        const user = auth.currentUser;
        if (user) {
          const userRef = doc(db, "users", user.uid);
          await setDoc(userRef, { username: userName });

          // Create initial tabs for the user
          const tabsRef = collection(db, "users", user.uid, "tabs");
          const initialTabs = ["Year 1", "Year 2", "Year 3"];
          initialTabs.forEach(async (tab) => {
            await addDoc(tabsRef, { name: tab });
          });

          // Adding the email verification
          await sendEmailVerification(auth.currentUser);
          toast.success(
            "A verification email has been sent to your email address. Please verify your email before logging in."
          );
        }

        console.log("Registered");
        setErrors({});
      } catch (error) {
        console.log("Registration failed with error:", error);
        if (error.code === "auth/email-already-in-use") {
          toast.error(
            "Registration failed, you may already have an account with this email"
          );
        } else {
          throw error; // Re-throw the error to be caught by the outer catch block
        }
      }
    }
  };

  //Checking to see if the context works --> IT WORKS
  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrors("");
    try {
      await createUser(email, password);
      navigate("/Dashboard");
    } catch (e) {
      setErrors(e.message);
      console.log(e.message);
    }
  };

  //Handle the input field change
  const handleUserNameChange = (e) => {
    setUserName(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  return (
    <Container>
      <Box>
        <ToastContainer />
        <RightSection>
          <RightBox>
            <TitleContainer>
              <Title>SIGNUP</Title>
              <SignupContainer>
                <DHAA>Already have an account?</DHAA>
                <SignupLink to="/Login">Login.</SignupLink>
              </SignupContainer>
            </TitleContainer>
            <Form>
              <EmailContainer>
                <EmailText>Username</EmailText>
                <input
                  style={{
                    width: "100%",
                    color: "#0f0f0f",
                    backgroundColor: "transparent",
                    borderTop: "0px",
                    borderLeft: "0px",
                    borderRight: "0px",
                    borderBottom: "1px solid #0f0f0f",
                    outline: "none",
                    padding: "15px 0px",
                    display: "flex",
                    fontSize: "14px",
                  }}
                  type="text"
                  placeholder="What should we call you?"
                  value={userName}
                  name="userName"
                  onChange={handleUserNameChange} // Updated
                />
              </EmailContainer>
              <EmailContainer>
                <EmailText>Email</EmailText>
                <input
                  style={{
                    width: "100%",
                    color: "#0f0f0f",
                    backgroundColor: "transparent",
                    borderTop: "0px",
                    borderLeft: "0px",
                    borderRight: "0px",
                    borderBottom: "1px solid #0f0f0f",
                    outline: "none",
                    padding: "15px 0px",
                    display: "flex",
                    fontSize: "14px",
                  }}
                  name="email"
                  value={email}
                  type="email"
                  placeholder="Enter your email"
                  onChange={handleEmailChange} // Updated
                />
              </EmailContainer>
              <PasswordContainer>
                <PasswordText>Password</PasswordText>
                <PInputContainer>
                  <input
                    style={{
                      width: "90%",
                      color: "#0f0f0f",
                      backgroundColor: "transparent",
                      borderTop: "0px",
                      borderLeft: "0px",
                      borderRight: "0px",
                      borderBottom: "1px solid #0f0f0f",
                      outline: "none",
                      padding: "15px 0px",
                      display: "flex",
                      fontSize: "14px",
                    }}
                    name="password"
                    value={password}
                    onChange={handlePasswordChange} // Updated
                    type={showPassword ? "text" : "password"}
                    placeholder="Enter your password"
                  />
                  {showPassword ? (
                    <RiEyeOffLine onClick={handleToggle} />
                  ) : (
                    <RiEyeLine onClick={handleToggle} />
                  )}
                </PInputContainer>
              </PasswordContainer>
              <PasswordContainer>
                <PasswordText>Retype Password</PasswordText>
                <PInputContainer>
                  <input
                    style={{
                      width: "90%",
                      color: "#0f0f0f",
                      backgroundColor: "transparent",
                      borderTop: "0px",
                      borderLeft: "0px",
                      borderRight: "0px",
                      borderBottom: "1px solid #0f0f0f",
                      outline: "none",
                      padding: "15px 0px",
                      display: "flex",
                      fontSize: "14px",
                    }}
                    name="confirmpassword"
                    value={confirmPassword}
                    onChange={handleConfirmPasswordChange} // Updated
                    type={showRePassword ? "text" : "password"}
                    placeholder="Re-enter your password"
                  />
                  {showRePassword ? (
                    <RiEyeOffLine onClick={handleToggleRe} />
                  ) : (
                    <RiEyeLine onClick={handleToggleRe} />
                  )}
                </PInputContainer>
              </PasswordContainer>
            </Form>
            <ButtonContainer>
              <LoginButton onClick={handleRegister}>SignUp</LoginButton>
            </ButtonContainer>
          </RightBox>
        </RightSection>
        <LeftSection>
          <LeftBox>
            <LogoLink to="/">MEDMATCHR</LogoLink>
            <InformationContainer>
              <Intro>START YOUR JOURNEY</Intro>
              <SubIntro>
                The path to medschool made easier, discover what lies ahead!
              </SubIntro>
            </InformationContainer>
            <QuoteContainer>
              <QuoteBox>
                <Quote>
                  The path to medical school may be daunting, but it's an
                  opportunity to learn, grow, and discover the incredible impact
                  you can have on patients' lives.
                </Quote>
                <AuthorContainer>
                  <AuthorPicContainer>
                    <AuthorPic src={AR} />
                  </AuthorPicContainer>
                  <AuthorBio>
                    <Name>Dr. Amanda Rodriguez</Name>
                    <JobTitle>Pediatrician</JobTitle>
                  </AuthorBio>
                </AuthorContainer>
              </QuoteBox>
            </QuoteContainer>
          </LeftBox>
        </LeftSection>
      </Box>
    </Container>
  );
}
