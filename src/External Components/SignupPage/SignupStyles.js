//Import styled from styled-components
import { styled } from "styled-components";

//Importing the Link
import { Link } from "react-router-dom";

//Styling exports
export const Container = styled.div`
  width: 100%;
  height: 100vh;
  background-color: #0f0f0f;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: default;
  font-family: Montserrat-Alt1;

  ::placeholder {
    color: #595959;
  }
`;

export const Box = styled.div`
  width: 1000px;
  display: flex;
  background-color: #f1eed3;
  height: 650px;
  border-radius: 25px;

  @media screen and (max-width: 1000px) {
    height: 100%;
    border-radius: 0px;
  }
`;

export const LeftSection = styled.div`
  width: 40%;
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

export const LeftBox = styled.div`
  background-color: #0f0f0f;
  height: 97%;
  width: 94%;
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const LogoLink = styled(Link)`
  font-weight: bold;
  font-size: 22px;
  color: #f1eed3;
  margin-left: 25px;
  margin-top: 25px;
  width: fit-content;
  cursor: pointer;
  text-decoration: none;
`;

export const InformationContainer = styled.div`
  margin-left: 25px;
  display: flex;
  flex-direction: column;
  gap: 30px;
`;

export const Intro = styled.div`
  color: #f1eed3;
  font-size: 35px;
  font-weight: bold;
`;

export const SubIntro = styled.div`
  color: #76756a;
  line-height: 20px;
  font-size: 18px;
`;

export const QuoteContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const QuoteBox = styled.div`
  background-color: #f1eed3;
  width: 90%;
  height: 180px;
  border-radius: 20px;
  margin-bottom: 30px;
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;
  align-items: center;
  gap: 12px;
`;

export const Quote = styled.div`
  color: #0f0f0f;
  font-size: 14px;
  line-height: 17px;
  width: 95%;
  font-weight: bold;
`;

export const AuthorContainer = styled.div`
  display: flex;
  gap: 15px;
  width: 95%;
  align-items: center;
`;

export const AuthorPicContainer = styled.div`
  padding: 20px;
  height: 20px;
  width: 20px;
  background-color: white;
  border-radius: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  border: 2px solid #0f0f0f;
`;

export const AuthorPic = styled.img`
  width: 250px;
  height: auto;
  margin-top: 65px;
  margin-right: 45px;
`;

export const AuthorBio = styled.div`
  font-size: 14px;
  display: flex;
  flex-direction: column;
  gap: 5px;
`;

export const Name = styled.div`
  color: #0f0f0f;
  font-weight: bold;
`;

export const JobTitle = styled.div`
  font-size: 12px;
  color: #76756a;
`;

export const RightSection = styled.div`
  width: 60%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 1000px) {
    width: 100%;
  }
`;

export const RightBox = styled.div`
  height: 500px;
  width: 85%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 25px;
`;

export const Title = styled.div`
  font-weight: bold;
  font-size: 22px;
  color: #0f0f0f;
  margin-top: 25px;
  width: fit-content;
  cursor: default;
  text-decoration: none;
`;

export const SignupContainer = styled.div`
  display: flex;
  gap: 3px;

  @media screen and (max-width: 272px) {
    flex-direction: column;
    
  }
`;

export const DHAA = styled.div`
  font-size: 15px;
`;

export const SignupLink = styled(Link)`
  text-decoration: none;
  font-size: 15px;
  color: #0f0f0f;
  font-weight: bold;
  transition: ease-in-out 0.15s;

  &:hover {
    translate: 5px;
  }
`;

export const Form = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 20px;
`;

export const EmailContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const EmailText = styled.div`
  font-weight: bold;
`;

export const PasswordContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const PInputContainer = styled.div`
  display: flex;
  gap: 20px;
  align-items: center;
  justify-content: start;
`;

export const PasswordText = styled.div`
  font-weight: bold;
`;

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;

  @media screen and (max-width: 385px) {
    justify-content: center;
  }
`;

export const LoginButton = styled.div`
  padding: 20px 50px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  border-radius: 25px;
  font-weight: bold;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: 2px solid #f1eed3;
  width: max-content;
  transition: ease-in-out 0.3s;

  &:hover {
    background-color: transparent;
    border: 2px solid #0f0f0f;
    color: #0f0f0f;
    cursor: pointer;
  }
`;
