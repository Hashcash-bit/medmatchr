//Import the react dependencies
import React, { useState } from "react";

// Styled components for the login page
import {
  AuthorBio,
  AuthorContainer,
  AuthorPic,
  Box,
  ButtonContainer,
  Container,
  DHAA,
  Form,
  InformationContainer,
  Intro,
  JobTitle,
  LeftBox,
  LeftSection,
  LoginButton,
  LogoLink,
  Name,
  Quote,
  QuoteBox,
  QuoteContainer,
  RightSection,
  SignupContainer,
  SignupLink,
  SubIntro,
  Title,
  TitleContainer,
  AuthorPicContainer,
  RightBox,
  EmailContainer,
  EmailText,
  PasswordContainer,
  PasswordText,
  PInputContainer,
} from "./LoginStyles";

//Importing the image
import DC from "./img/DC.jpg";

//Importing the icons
import { RiEyeLine, RiEyeOffLine } from "react-icons/ri";

//Importing the Routing
import { useNavigate } from "react-router-dom";

//Importing the Toasts
import { ToastContainer, toast } from "react-toastify"; // Import react-toastify components
import "react-toastify/dist/ReactToastify.css"; // Import react-toastify CSS file

//Importing the firebase dependencies
import { auth } from "../firebase/Firebase";
import { signInWithEmailAndPassword } from "firebase/auth";
import { UserAuth } from "../../Context/AuthContext";

// The main component for the login and sign up page
export default function Login() {
  // State variables for the email, password, theme and password visibility
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useNavigate();
  const navigate = useNavigate();
  const { signIn, username, sendPasswordResetEmail } = UserAuth();

  //Toggle the password view
  const [showPassword, setShowPassword] = useState(false);

  const handleToggle = () => {
    setShowPassword(!showPassword);
  };

  //Time to do some error handling
  const [showErrors, setShowErrors] = useState(false);
  const [errors, setErrors] = useState({});

  const getErrors = (email, password) => {
    const errors = {};

    // This is the error handling for the email
    if (!email) {
      errors.email = "Please enter your email";
    } else if (
      typeof email === "string" &&
      (!email.includes("@") || !email.includes(".com"))
    ) {
      errors.email = "Please enter a proper email (johndoe@whatever.com)";
    }

    if (!password) {
      errors.password = "Please enter your password";
    }

    return errors;
  };

  const handleLogin = async (e) => {
    e.preventDefault();
    const errors = getErrors(email, password);

    if (Object.keys(errors).length > 0) {
      setShowErrors(true);
      setErrors(errors);

      Object.values(errors).forEach((errorMsg) => {
        // Use toast.error to display error messages
        toast.error(errorMsg);
      });
    } else {
      try {
        await signIn(email, password);
        // Direct after successful Login
        // Use toast.success to display success messages
        toast.success("You are now logged in!");
        navigate(`/Dashboard/${username}`);
        console.log("Logged In");
        setErrors({});
      } catch (error) {
        console.log("Login failed with error:", error);
        // Use toast.error to display error messages
        toast.error(
          "Login failed. Please check you credentials, you may not have and account."
        );
      }
    }
  };

  //Testing to see if the auth context work --> IT WORKS
  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrors("");
    try {
      await signIn(email, password);
      navigate("/Dashboard");
    } catch (e) {
      setErrors(e.message);
      console.log(e.message);
    }
  };

  //Handle the input field change
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  //This is where the handle forgot password will go
  const handleForgotPassword = () => {
    if (email) {
      sendPasswordResetEmail(email)
        .then(() => {
          toast.success("Password reset link sent");
        })
        .catch((error) => {
          // Handle any errors that may occur
        });
    } else {
      toast.error("Please enter your email first");
    }
  };

  return (
    <Container>
      <Box>
        <ToastContainer />
        <LeftSection>
          <LeftBox>
            <LogoLink to="/">MEDMATCHR</LogoLink>
            <InformationContainer>
              <Intro>CONTINUE YOUR JOURNEY</Intro>
              <SubIntro>
                The path to medschool made easier, discover what lies ahead!
              </SubIntro>
            </InformationContainer>
            <QuoteContainer>
              <QuoteBox>
                <Quote>
                  The journey to becoming a doctor is long and challenging, but
                  remember to find joy in the process and stay true to your
                  passion for healing.
                </Quote>
                <AuthorContainer>
                  <AuthorPicContainer>
                    <AuthorPic src={DC} />
                  </AuthorPicContainer>
                  <AuthorBio>
                    <Name>Dr. David Chen</Name>
                    <JobTitle>Family Medicine Practitioner</JobTitle>
                  </AuthorBio>
                </AuthorContainer>
              </QuoteBox>
            </QuoteContainer>
          </LeftBox>
        </LeftSection>
        <RightSection>
          <RightBox>
            <TitleContainer>
              <Title>LOGIN</Title>
              <SignupContainer>
                <DHAA>Don't have an account?</DHAA>
                <SignupLink to="/Signup">Signup.</SignupLink>
              </SignupContainer>
            </TitleContainer>
            <Form>
              <EmailContainer>
                <EmailText>Email</EmailText>
                <input
                  style={{
                    width: "100%",
                    color: "#0f0f0f",
                    backgroundColor: "transparent",
                    borderTop: "0px",
                    borderLeft: "0px",
                    borderRight: "0px",
                    borderBottom: "1px solid #0f0f0f",
                    outline: "none",
                    padding: "15px 0px",
                    display: "flex",
                    fontSize: "14px",
                  }}
                  name="email"
                  onChange={handleEmailChange}
                  value={email}
                  type="email"
                  placeholder="Enter your email"
                />
              </EmailContainer>
              <PasswordContainer>
                <PasswordText>Password</PasswordText>
                <PInputContainer>
                  <input
                    style={{
                      width: "90%",
                      color: "#0f0f0f",
                      backgroundColor: "transparent",
                      borderTop: "0px",
                      borderLeft: "0px",
                      borderRight: "0px",
                      borderBottom: "1px solid #0f0f0f",
                      outline: "none",
                      padding: "15px 0px",
                      display: "flex",
                      fontSize: "14px",
                    }}
                    name="password"
                    onChange={handlePasswordChange}
                    value={password}
                    type={showPassword ? "text" : "password"}
                    placeholder="Enter your password"
                  />
                  {showPassword ? (
                    <RiEyeOffLine onClick={handleToggle} />
                  ) : (
                    <RiEyeLine onClick={handleToggle} />
                  )}
                </PInputContainer>
              </PasswordContainer>
            </Form>
            <div
              style={{
                fontSize: "12px",
                width: "fit-content",
                opacity: "0.8",
                fontWeight: "600",
                cursor: "pointer",
              }}
              onClick={handleForgotPassword}
            >
              Forgot Passwords?
            </div>
            <ButtonContainer>
              <LoginButton onClick={handleLogin}>LogIn</LoginButton>
            </ButtonContainer>
          </RightBox>
        </RightSection>
      </Box>
    </Container>
  );
}
