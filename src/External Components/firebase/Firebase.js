// Import the Firebase SDK and the services you want to use
import { initializeApp } from "firebase/app";
import { getFirestore, collection } from "firebase/firestore"; // Import Firestore
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage"; // Import Storage

// Your web app's Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCRLr4pwbEaPWmv64EK--6h416ASBXvPLU",
  authDomain: "medmatchr.firebaseapp.com",
  projectId: "medmatchr",
  storageBucket: "medmatchr.appspot.com",
  messagingSenderId: "503913896077",
  appId: "1:503913896077:web:ec7251dd3a15c127832ac4",
  measurementId: "G-YVJC4MWW32",
};

// Initialize Firebase with the configuration object
const app = initializeApp(firebaseConfig);

// Create a Firestore instance
const db = getFirestore(app);

// Create a Storage instance
const storage = getStorage(app);

// Export the app, the auth, and the db services that we want to use in other files
export const auth = getAuth(app);
export { db, storage };
