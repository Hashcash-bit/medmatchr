import { useState } from "react";
import { UserAuth } from "../Context/AuthContext";
import { doc, updateDoc } from "firebase/firestore";
import { db } from "../External Components/firebase/Firebase";
import styled from "styled-components";

const ModalContainer = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  padding: 20px;
  background-color: #f1eed3;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
  height: 200px;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
  border-radius: 25px;
  font-family: Montserrat-Alt1;
`;

const ModalBackdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.7);
  filter: blur(2px);
`;

const Input = styled.input`
  padding: 10px;
  border-radius: 10px;
`;

const Button = styled.button`
  margin: 10px;
  border-radius: 10px;
  padding: 10px 20px;
  transition: ease-in-out 0.2s;

  &:hover {
    cursor: pointer;
    font-size: 15px;
  }
`;

const UsernameModal = ({ isOpen, onClose }) => {
  const [newUsername, setNewUsername] = useState("");
  const { user } = UserAuth();

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const userRef = doc(db, "users", user.uid);
      await updateDoc(userRef, { username: newUsername });
      onClose();
      window.location.reload();
    } catch (error) {
      console.error(error);
    }
  };

  if (!isOpen) return null;

  return (
    <>
      <ModalBackdrop />
      <ModalContainer>
        <form onSubmit={handleSubmit}>
          <label>
            New Username:
            <Input
              type="text"
              value={newUsername}
              onChange={(event) => setNewUsername(event.target.value)}
            />
          </label>
          <Button type="submit">Save</Button>
        </form>
        <Button onClick={onClose}>Cancel</Button>
      </ModalContainer>
    </>
  );
};

export default UsernameModal;
