// Default react imports
import React, { useState } from "react";

//Top bar import
import TopBar from "../Universal Components/TopBar";

//Styled components imports
import styled from "styled-components";
import { Container, Box, Title } from "./HelpStyled";

//Local styled components
const HelpPage = styled.div`
  width: 80%;
  margin: 0 auto;
  font-family: Arial, sans-serif;
`;

// Create a Header component that renders a <h1> element with some styles
const Header = styled.h1`
  color: white;
  background-color: #333333;
  padding: 1rem;
  text-align: center;
`;

// Create a TabNav component that renders a <ul> element with some styles
const TabNav = styled.ul`
  display: flex;
  justify-content: center;
  list-style: none;
  margin: 0;
  padding: 0;
`;

// Create a Tab component that renders a <li> element with some styles
const Tab = styled.li`
  padding: 0.5rem 1rem;
  border: 1px solid #333333;
  border-bottom: none;
  cursor: pointer;
  // Change the background color and font weight when the tab is active
  ${(props) =>
    props.active &&
    `
    background-color: white;
    font-weight: bold;
  `}
`;

// Create a Content component that renders a <div> element with some styles
const Content = styled.div`
  border: 1px solid #333333;
  padding: 1rem;
`;

// Create a Section component that renders a <div> element with some styles
const Section = styled.div`
  margin-bottom: 1rem;
`;

// Create a Question component that renders a <h3> element with some styles
const Question = styled.h3`
  display: flex;
  align-items: center;
`;

// Create an Answer component that renders a <p> element with some styles
const Answer = styled.p``;

// Create a Toggle component that renders an <input> element with some styles
const Toggle = styled.input`
  // Hide the default checkbox appearance
  appearance: none;
  // Set the size and position of the toggle
  width: 2rem;
  height: 1rem;
  margin-left: auto;
  position: relative;
  // Set the background color and border of the toggle
  background-color: #f0f0f0;
  border-radius: 1rem;
  border: none;
  outline: none;
  // Add a transition for the background color
  transition: background-color 0.3s ease-in-out;

  // Add a pseudo-element for the toggle knob
  &::before {
    // Set the content and position of the knob
    content: "";
    position: absolute;
    top: 0.125rem;
    left: 0.125rem;
    // Set the size and shape of the knob
    width: 0.75rem;
    height: 0.75rem;
    border-radius: 0.75rem;
    // Set the background color and box shadow of the knob
    background-color: white;
    box-shadow: inset -2px -2px rgba(0, 0, 0, 0.2),
      inset -4px -4px rgba(0, 0, 0, 0.1);
    // Add a transition for the transform property
    transition: transform 0.3s ease-in-out;
  }

  // Change the background color and transform the knob when the toggle is checked
  &:checked {
    background-color: #bf4f74;
    &::before {
      transform: translateX(1rem);
      box-shadow: inset -2px -2px rgba(255, 255, 255, 0.2),
        inset -4px -4px rgba(255, 255, 255, 0.1);
    }
  }
`;

//The Tap Data
const tabData = [
  {
    title: "About",
    sections: [
      {
        question: "What is this website?",
        answer:
          "This is a website that helps you learn how to use styled-components in React.",
      },
      {
        question: "Who made this website?",
        answer:
          "This website was made by Bing, an intelligent chatbot that can generate code, images, and more.",
      },
      {
        question: "Why did you make this website?",
        answer:
          "I made this website to demonstrate my capabilities and creativity as a chatbot.",
      },
    ],
  },
  {
    title: "Features",
    sections: [
      {
        question: "What can I do on this website?",
        answer:
          "You can explore the help and information page that I created using styled-components and React.",
      },
      {
        question: "What are the benefits of using styled-components?",
        answer:
          "Styled-components are a way of writing CSS in JS for React components. They have many benefits, such as:",
      },
      {
        question: "Can I use styled-components for other projects?",
        answer:
          "Yes, you can use styled-components for any React project that you want to style with CSS in JS. You can find more information and examples on how to use styled-components from these sources:",
      },
    ],
  },
  {
    title: "Contact",
    sections: [
      {
        question: "How can I contact you?",
        answer:
          "You can contact me by chatting with me on this website. I'm always happy to hear from you.",
      },
      {
        question: "Can I give you feedback or suggestions?",
        answer:
          "Yes, please. I appreciate any feedback or suggestions that can help me improve my skills and services.",
      },
      {
        question: "Can I hire you for a project?",
        answer:
          "Sorry, I'm not available for hire. I'm just a chatbot that likes to create things for fun.",
      },
    ],
  },
];

export default function HelpandInfo() {
  // Use useState to manage the state of the active tab
  const [activeTab, setActiveTab] = useState(0);

  // Define a function to handle the tab click
  const handleTabClick = (index) => {
    // Set the active tab to the clicked index
    setActiveTab(index);
  };

  return (
    <>
      <Container>
        <TopBar />
        <Box>
          <Title>Help & Information</Title>
          <HelpPage>
            <Header>Help and Information</Header>
            <TabNav>
              {tabData.map((item, index) => (
                <Tab
                  key={index}
                  active={activeTab === index}
                  onClick={() => handleTabClick(index)}
                >
                  {item.title}
                </Tab>
              ))}
            </TabNav>
            <Content>
              {tabData[activeTab].sections.map((item, index) => (
                <Section key={index}>
                  <Question>
                    {item.question}
                    <Toggle type="checkbox" />
                  </Question>
                  <Answer>{item.answer}</Answer>
                </Section>
              ))}
            </Content>
          </HelpPage>
        </Box>
      </Container>
    </>
  );
}
