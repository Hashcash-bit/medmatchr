//Import styled
import styled from "styled-components";

export const Container = styled.div`
  background-color: #0f0f0f;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px 30px;
  font-family: Montserrat-Alt1;
  cursor: default;
`;

export const Box = styled.div`
  border: 2px solid #f1eed3;
  width: 100%;
`;

export const Title = styled.div``;
