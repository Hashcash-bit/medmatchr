import React, { useState } from "react";
import styled from "styled-components";

const DropdownContainer = styled.div`
  position: relative;
  width: max-content;
`;

const DropdownButton = styled.div`
  background-color: #1d1d1d;
  cursor: pointer;
  width: 200px;
  color: #f1eed3;

  display: flex;
  padding: 10px 10px;
  gap: 10px;
  border-radius: 25px;
  align-items: center;
  justify-content: center;
`;

const DropdownItems = styled.div`
  font-size: 15px;
  height: min-content;
  width: 100%;
  font-weight: 600;
  color: #f1eed3;
  cursor: pointer;
  transition: ease-in-out 0.2s;
  height: 100%;
  padding-top: 2.5px;
  padding-bottom: 2.5px;
  padding-left: 5px;
  padding-right: 5px;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

const DropdownList = styled.ul`
  position: absolute;
  top: 100%;
  left: 0;
  width: 100%;
  max-height: 200px;
  overflow-y: auto;
  background-color: #0f0f0f;
  border: 1px solid #f1eed3;
  border-radius: 10px;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
  list-style: none;
  padding: 0;
  margin: 0;
  z-index: 10;
`;

const DropdownListItem = styled.li`
  padding: 10px;
  cursor: pointer;
  color: #f1eed3;
  font-size: 15px;

  &:hover {
    background-color: #222222;
  }
`;

const Dropdown = () => {
  const [selectedOption, setSelectedOption] = useState(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const options = [
    "McMaster University",
    "McGill University",
    "Waterloo University",
  ];

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const selectOption = (option) => {
    setSelectedOption(option);
    setDropdownOpen(false);
  };

  return (
    <DropdownContainer>
      <DropdownButton onClick={toggleDropdown}>
        <DropdownItems>{selectedOption || "Institution ▾"}</DropdownItems>
      </DropdownButton>
      {dropdownOpen && (
        <DropdownList>
          {options.map((option) => (
            <DropdownListItem key={option} onClick={() => selectOption(option)}>
              {option}
            </DropdownListItem>
          ))}
        </DropdownList>
      )}
    </DropdownContainer>
  );
};

export default Dropdown;
