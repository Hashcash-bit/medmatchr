//Default react imports
import React, { useState, useEffect } from "react";

// CSS import
import "./ScheduleStyled.css";

// Icons Imports
import { BiTrash } from "react-icons/bi";

// The course Data
import courses from "../../External Components/OnBoarding/Components/Utils/Course";

// The medschool Data
import medSchools from "../Dashboard/Components/Schedule/Utils/Requirements";

//Toast
import { ToastContainer, toast } from "react-toastify";

// The Auth context
import { UserAuth } from "../../Context/AuthContext";

// The Top bar
import TopBar from "../Universal Components/TopBar";

// Styled content
import {
  Container,
  Content,
  ContentWrapper,
  Input,
  SelectCourse,
  Subtext,
  Tab,
  TabItems,
  TabNavigator,
  Title,
  Wrapper,
  YearIndicator,
  Highlight,
  Popup,
  PopupContent,
  CourseList,
  CourseItem,
  AddButton,
  EnterButton,
  InputContainer,
  List,
  Item,
  ResultContainer,
  ResultItem,
  AddButtonContainer,
  CourseListing,
  CourseBox,
  LeftCourseBox,
  RightCourseBox,
  CoursePrefixContainer,
  CourseCodeContainer,
  CourseTermContainer,
  CourseClassificationContainer,
  CourseNameContainer,
  RemoveButton,
  CrossesImage,
  RigthSub,
  LeftSub,
  SubCourseBox,
  RemoveTabButton,
  YearTab,
  CourseBoxM,
  SubCourseBoxM,
  LeftCourseBoxM,
  CoursePrefixContainerM,
  CourseCodeContainerM,
  RightCourseBoxM,
  LeftSubM,
  RigthSubM,
  CourseNameContainerM,
  CourseClassificationContainerM,
  RemoveButtonM,
  CheckEligebilityTitle,
  CheckEligebilityContainer,
  CheckEligibilityButton,
  MedSchoolBox,
  MedSchoolName,
  MedSchoolGPA,
  MedSchoolPrequisitesTitle,
  PrereqList,
  ListItems,
  BoxTopContainer,
  BoxBottomContainer,
  SubBox,
  MedSchools,
  MedSchoolScrollBox,
  MedSchoolImage,
  MedSchoolImageContainer,
  MedschoolCounter,
  MedSchoolsM,
  SubBoxM,
  BoxTopContainerM,
  MedSchoolNameM,
  MedSchoolGPAM,
  MedSchoolImageContainerM,
  MedSchoolImageM,
  BoxBottomContainerM,
  MedSchoolPrequisitesTitleM,
  PrereqListM,
  ListItemsM,
  TabM,
  YearTabM,
  RemoveTabButtonM,
  SelectCourseM,
  SquiglyArrow,
} from "./SchedulePageStyles";

// The Images
import Hat from "../Img/hat.png";
import SArrow from "../Img/SquiglyArrow.svg";

// Firebase Imports
import {
  doc,
  setDoc,
  collection,
  addDoc,
  onSnapshot,
  getDocs,
  where,
  query,
  deleteDoc,
} from "firebase/firestore";
import { db } from "../../External Components/firebase/Firebase";

export default function SchedulePage() {
  const [years, setYears] = useState(["Year 1", "Year 2", "Year 3"]);
  const [selectedYear, setSelectedYear] = useState("Year 1");
  const [term, setTerm] = useState({});
  const [showPopup, setShowPopup] = useState(false);

  // The Auth context
  const { user, username } = UserAuth();

  // THe new constants here
  const [searchTerm, setSearchTerm] = useState(""); //This is the search term that will be used to filter the courses
  const [eligibleSchools, setEligibleSchools] = useState([]);
  const [universityFilter, setUniversityFilter] = useState("");
  const [selectedCourses, setSelectedCourses] = useState([]);
  const [savedCourses, setSavedCourses] = useState([]);
  const [results, setResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [firebaseCourses, setFirebaseCourses] = useState([]);

  // Moockup Tabs
  const MockupTabs = () => (
    <div
      style={{
        display: "flex",
        cursor: "not-allowed",
        opacity: "0.5",
      }}
    >
      <TabM>
        <YearTabM>Year 1</YearTabM>
        <RemoveTabButtonM>
          <BiTrash />
        </RemoveTabButtonM>
      </TabM>
      <TabM>
        <YearTabM>Year 2</YearTabM>
        <RemoveTabButtonM>
          <BiTrash />
        </RemoveTabButtonM>
      </TabM>
      <TabM>
        <YearTabM>Year 3</YearTabM>
        <RemoveTabButtonM>
          <BiTrash />
        </RemoveTabButtonM>
      </TabM>
    </div>
  );

  // Mockup Course
  const MockupCourse = () => (
    <CourseBoxM>
      <SubCourseBoxM>
        <LeftCourseBoxM>
          <CoursePrefixContainerM>HTHSCI</CoursePrefixContainerM>
          <CourseCodeContainerM>1AAA</CourseCodeContainerM>
        </LeftCourseBoxM>
        <RightCourseBoxM>
          <LeftSubM>
            <div>Terms</div>
            <CourseTermContainer>1</CourseTermContainer>
          </LeftSubM>
          <RigthSubM>
            <CourseNameContainerM>
              Helping the Human Body Progress
            </CourseNameContainerM>
            <CourseClassificationContainerM>
              Advanced Biology
            </CourseClassificationContainerM>
          </RigthSubM>
        </RightCourseBoxM>
      </SubCourseBoxM>
      <RemoveButtonM>Remove</RemoveButtonM>
    </CourseBoxM>
  );

  // Mockup MedSchool
  const MockupMedSchool = () => (
    <>
      <MedSchoolsM>
        <SubBoxM>
          <BoxTopContainerM>
            <MedSchoolNameM>McMaster University</MedSchoolNameM>
            <MedSchoolGPAM>GPA: 3.8</MedSchoolGPAM>
          </BoxTopContainerM>
          <MedSchoolImageContainerM>
            <MedSchoolImageM src={Hat} />
          </MedSchoolImageContainerM>
          <BoxBottomContainerM>
            <MedSchoolPrequisitesTitleM>
              Prerequisite Requirements:
            </MedSchoolPrequisitesTitleM>
            <PrereqListM>
              <ListItemsM>Humanities: 1 Term</ListItemsM>
              <ListItemsM>Biology: 2 Terms</ListItemsM>
              <ListItemsM>Chemistry: 2 Terms</ListItemsM>
            </PrereqListM>
          </BoxBottomContainerM>
        </SubBoxM>
      </MedSchoolsM>
    </>
  );

  // The new code
  // This is where the highlight logic will go
  const highlightSearchTerm = (text) => {
    const parts = text.split(new RegExp(`(${searchTerm})`, "gi"));
    return (
      <>
        {parts.map((part, i) =>
          part.toLowerCase() === searchTerm.toLowerCase() ? (
            <Highlight key={i}>{part}</Highlight>
          ) : (
            part
          )
        )}
      </>
    );
  };

  //I will be adding a custom Search functionality here the onSubmit
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!searchTerm) {
      toast.error("Please enter a search term");
      return;
    }
    let filteredCourses = courses.filter((course) => {
      if (universityFilter && course.university !== universityFilter) {
        return false;
      }
      return (
        course.university.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.prefix.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.code.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.classification.toLowerCase().includes(searchTerm.toLowerCase())
      );
    });
    setResults(filteredCourses);
  };

  // This where the isSavedCourse logic will go --> so when I add the course it will trigger the is Saved Course so that the add button goes away.
  const isCourseSaved = async (course) => {
    // Checking the state of the saved courses in firebase
    const coursesRef = collection(db, "users", user.uid, "courses");
    const querySnapshot = await getDocs(
      query(
        coursesRef,
        where("name", "==", course.name) &&
          where("prefix", "==", course.prefix),
        where("code", "==", course.code)
      )
    );

    return !querySnapshot.empty;
  };

  // Adding the useEffect for the isCourseSaved
  useEffect(() => {
    const checkSavedCourses = async () => {
      const savedCourses = await Promise.all(
        results.map(async (result) => await isCourseSaved(result))
      );
      setSavedCourses(savedCourses);
    };
    checkSavedCourses();
  }, [results]);

  //I will be adding the check eligebility functionality here
  const checkEligibility = () => {
    const eligible = medSchools.filter((school) => {
      return school.prerequisites.every((prereq) => {
        const totalTerms = selectedCourses.reduce((acc, course) => {
          if (course.classification === prereq.classification) {
            return acc + course.terms;
          }
          return acc;
        }, 0);
        return totalTerms >= prereq.terms;
      });
    });
    setEligibleSchools(eligible);
  };

  //Old Code
  //Adding the year in the tab
  const handleAddYear = async () => {
    const newYearName = prompt("Enter the name of the new year:");
    if (newYearName) {
      setYears((prevYears) => [...prevYears, newYearName]);

      // This is where the tab will be saved in the databse
      const tabsRef = collection(db, "users", user.uid, "tabs");
      await addDoc(tabsRef, { name: newYearName });
    }
  };

  const handleAddCourse = async (course) => {
    setTerm((prevCourses) => ({
      ...prevCourses,
      [selectedYear]: [...(prevCourses[selectedYear] || []), course],
    }));
    setSelectedCourses((prevCourses) => [...prevCourses, course]);

    // Save course to Firestore
    console.log(
      "Arguments passed to collection:",
      db,
      "users",
      user.uid,
      "courses"
    );
    const coursesRef = collection(db, "users", user.uid, "courses");
    await addDoc(coursesRef, {
      name: course.name,
      prefix: course.prefix,
      code: course.code,
      terms: course.terms,
      classification: course.classification,
      year: selectedYear, // Set the year property of the course
    });
    setShowPopup(false);
    setSearchTerm("");
  };

  const handleRemoveCourse = async (courseIndex) => {
    // Get the course to be removed
    const courseToRemove = term[selectedYear][courseIndex];

    // Remove course from local state
    setTerm((prevCourses) => ({
      ...prevCourses,
      [selectedYear]: prevCourses[selectedYear].filter(
        (_, index) => index !== courseIndex
      ),
    }));
    setSelectedCourses((prevCourses) =>
      prevCourses.filter((_, index) => index !== courseIndex)
    );

    // Remove course from Firestore
    console.log(
      "Arguments passed to collection:",
      db,
      "users",
      user.uid,
      "courses"
    );
    const coursesRef = collection(db, "users", user.uid, "courses");
    const querySnapshot = await getDocs(
      query(coursesRef, where("name", "==", courseToRemove.name))
    );
    querySnapshot.forEach((doc) => {
      deleteDoc(doc.ref);
    });
  };

  // Handle the tab removal
  const handleRemoveTab = async (yearToRemove) => {
    // Remove the tab from the local state
    setYears((prevYears) => prevYears.filter((year) => year !== yearToRemove));
    setTerm((prevTerm) => {
      const newTerm = { ...prevTerm };
      delete newTerm[yearToRemove];
      return newTerm;
    });
    setSelectedCourses((prevCourses) =>
      prevCourses.filter((course) => !term[yearToRemove].includes(course))
    );

    // Remove the tab from firebase
    const tabsRef = collection(db, "users", user.uid, "tabs");
    const tabsQuery = query(tabsRef, where("name", "==", yearToRemove));
    const tabsSnapshot = await getDocs(tabsQuery);
    tabsSnapshot.forEach((doc) => {
      deleteDoc(doc.ref);
    });

    // Remove the associated courses from firebase
    const coursesRef = collection(db, "users", user.uid, "courses");
    const coursesQuery = query(coursesRef, where("year", "==", yearToRemove));
    const coursesSnapshot = await getDocs(coursesQuery);
    coursesSnapshot.forEach((doc) => {
      deleteDoc(doc.ref);
    });
  };

  //This is going to retrieve the courses data from firebase
  const retrieveCourses = async () => {
    if (!user || !user.uid) {
      return;
    }

    // Retrieve the tabs from Firestore
    const tabsRef = collection(db, "users", user.uid, "tabs");
    const tabsSnapshot = await getDocs(tabsRef);
    const tabs = tabsSnapshot.docs.map((doc) => doc.data().name);

    // Update the years state variable
    setYears(tabs);

    // Retrieve the courses from Firestore
    const coursesRef = collection(db, "users", user.uid, "courses");
    const querySnapshot = await getDocs(coursesRef);
    const firebaseCourses = querySnapshot.docs.map((doc) => doc.data());

    // Filter courses based on the selected year
    const coursesByYear = tabs.reduce((acc, year) => {
      acc[year] = firebaseCourses.filter((course) =>
        course.year === year ? true : false
      );
      return acc;
    }, {});

    // Update state with retrieved and filtered courses
    setFirebaseCourses(coursesByYear);
    setTerm(coursesByYear);
  };

  useEffect(() => {
    if (user && user.uid) {
      retrieveCourses();
      setIsLoading(false);
    }
  }, [user]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <ToastContainer />
      <Container>
        <TopBar />
        <Wrapper>
          <Title>Schedule</Title>
          <Subtext>
            This is where you will create your personal schedule, {username}.
          </Subtext>
          <Content>
            <TabNavigator>
              {years.length === 0 ? (
                <MockupTabs />
              ) : (
                years.map((year, index) => (
                  <Tab
                    key={year}
                    onClick={() => setSelectedYear(year)}
                    data-selected={selectedYear === year}
                  >
                    <YearTab>{year}</YearTab>
                    {index >= 0 && (
                      <RemoveTabButton onClick={() => handleRemoveTab(year)}>
                        <BiTrash />
                      </RemoveTabButton>
                    )}
                  </Tab>
                ))
              )}
            </TabNavigator>
            {years.length === 0 ? (
              <div
                style={{
                  display: "flex",
                  justifyContent: "start",
                  gap: " 20px",
                }}
              >
                <TabItems onClick={handleAddYear}>+</TabItems>
                <div
                  style={{
                    display: "flex",
                    gap: "10px",
                  }}
                >
                  <SquiglyArrow src={SArrow} />
                  <div
                    style={{
                      border: "3px solid #f1eed3",
                      paddingLeft: "10px",
                      paddingRight: "10px",
                      paddingBottom: "20px",
                      paddingTop: "20px",
                      borderRadius: "10px",
                      width: "100px",
                      fontSize: "12px",
                      fontWeight: "500",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      textAlign: "center",
                      lineHeight: "13px",
                      color: "#f1eed3",
                    }}
                  >
                    Press the + to add your first tab!
                  </div>
                </div>
              </div>
            ) : (
              <TabItems onClick={handleAddYear}>+</TabItems>
            )}
          </Content>
          <ContentWrapper>
            <YearIndicator>{selectedYear}</YearIndicator>
            <CourseListing>
              {term[selectedYear] && term[selectedYear].length > 0 ? (
                term[selectedYear].map((course, index) => (
                  <CourseBox key={index}>
                    <SubCourseBox>
                      <LeftCourseBox>
                        <CoursePrefixContainer>
                          {course.prefix}
                        </CoursePrefixContainer>
                        <CourseCodeContainer>{course.code}</CourseCodeContainer>
                      </LeftCourseBox>
                      <RightCourseBox>
                        <LeftSub>
                          <div> Terms</div>
                          <CourseTermContainer>
                            {course.terms}
                          </CourseTermContainer>
                        </LeftSub>
                        <RigthSub>
                          <CourseNameContainer>
                            {course.name}
                          </CourseNameContainer>
                          <CourseClassificationContainer>
                            {course.classification}
                          </CourseClassificationContainer>
                        </RigthSub>
                      </RightCourseBox>
                    </SubCourseBox>
                    <RemoveButton
                      className="remove-button"
                      onClick={() => handleRemoveCourse(index)}
                    >
                      Remove
                    </RemoveButton>
                  </CourseBox>
                ))
              ) : (
                <MockupCourse />
              )}
            </CourseListing>
            {years.length === 0 ? (
              <SelectCourseM>Add Course</SelectCourseM>
            ) : (
              <SelectCourse onClick={() => setShowPopup(true)}>
                Add Course
              </SelectCourse>
            )}
          </ContentWrapper>
          <CheckEligebilityContainer>
            <CheckEligebilityTitle>Eligible Medschools</CheckEligebilityTitle>
            <CheckEligibilityButton onClick={checkEligibility}>
              Check Eligibility
            </CheckEligibilityButton>
            {eligibleSchools.length > 0 && (
              <>
                <MedschoolCounter>
                  You are eligible for:{" "}
                  <strong style={{ paddingLeft: "3px" }}>
                    {" "}
                    {eligibleSchools.length} schools
                  </strong>
                </MedschoolCounter>
              </>
            )}
            <MedSchoolScrollBox>
              <MedSchoolBox>
                {eligibleSchools.length > 0 ? (
                  eligibleSchools.map((school) => (
                    <>
                      <MedSchools key={school.name}>
                        <SubBox>
                          <BoxTopContainer>
                            <MedSchoolName>{school.name}</MedSchoolName>
                            <MedSchoolGPA>
                              GPA: {school.gpaRequirement}
                            </MedSchoolGPA>
                          </BoxTopContainer>
                          <MedSchoolImageContainer>
                            <MedSchoolImage src={Hat} />
                          </MedSchoolImageContainer>
                          <BoxBottomContainer>
                            <MedSchoolPrequisitesTitle>
                              Prerequisite Requirements:
                            </MedSchoolPrequisitesTitle>
                            <PrereqList>
                              {school.prerequisites.map((prereq, index) => (
                                <ListItems key={index}>
                                  {prereq.classification}: {prereq.terms} terms
                                </ListItems>
                              ))}
                            </PrereqList>
                          </BoxBottomContainer>
                        </SubBox>
                      </MedSchools>
                    </>
                  ))
                ) : (
                  <MockupMedSchool />
                )}
              </MedSchoolBox>
            </MedSchoolScrollBox>
          </CheckEligebilityContainer>
          {showPopup && (
            <>
              <Popup onClick={() => setShowPopup(false)}>
                <PopupContent onClick={(e) => e.stopPropagation()}>
                  <InputContainer>
                    <Input
                      type="text"
                      placeholder="Enter course code, name or classification..."
                      value={searchTerm}
                      onChange={(e) => setSearchTerm(e.target.value)}
                    />
                    <EnterButton onClick={handleSubmit}>Search</EnterButton>
                  </InputContainer>
                  {results.length > 0 && (
                    <div
                      style={{
                        padding: "10px",
                        color: "#868577",
                      }}
                    >
                      Number of results:{" "}
                      <strong style={{ color: "#f1eed3" }}>
                        {results.length}
                      </strong>
                    </div>
                  )}
                  <List>
                    {results.map((result, index, course) => (
                      <Item key={index}>
                        <ResultContainer>
                          <ResultItem>
                            <strong>University:</strong>{" "}
                            {highlightSearchTerm(result.university)}
                          </ResultItem>
                          <ResultItem>
                            <strong>Prefix:</strong>{" "}
                            {highlightSearchTerm(result.prefix)}
                          </ResultItem>
                          <ResultItem>
                            <strong>Code:</strong>{" "}
                            {highlightSearchTerm(result.code)}
                          </ResultItem>
                          <ResultItem>
                            <strong>Name:</strong>{" "}
                            {highlightSearchTerm(result.name)}
                          </ResultItem>
                          <ResultItem>
                            <strong>Classification:</strong>{" "}
                            {highlightSearchTerm(result.classification)}
                          </ResultItem>
                          <ResultItem>
                            <strong>Lab:</strong>{" "}
                            {highlightSearchTerm(result.lab)}
                          </ResultItem>
                        </ResultContainer>
                        <AddButtonContainer>
                          {!savedCourses[index] && (
                            <AddButton onClick={() => handleAddCourse(result)}>
                              Add
                            </AddButton>
                          )}
                        </AddButtonContainer>
                      </Item>
                    ))}
                  </List>
                </PopupContent>
              </Popup>
            </>
          )}
        </Wrapper>
      </Container>
    </>
  );
}
