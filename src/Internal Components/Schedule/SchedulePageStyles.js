//styled components
import styled from "styled-components";

export const Container = styled.div`
  background-color: #0f0f0f;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px 30px;
  font-family: Montserrat-Alt1;
  cursor: default;
`;

export const Wrapper = styled.div`
  max-width: 2000px;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  display: flex;
  flex-direction: column;
  padding-top: 15px;
  justify-content: start;
  align-items: start;
  //   background-color: yellow;

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #141413; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const Title = styled.div`
  font-size: 35px;
  font-weight: 700;
  color: #f1eed3;
  //   margin-left: 30px;
`;

export const Subtext = styled.div`
  font-size: 15px;
  font-weight: 500;
  color: #616056;
  //   margin-left: 30px;
  padding: 10px 0px;
`;

// The tab navigator shit
export const Content = styled.div`
  display: flex;
  align-items: center;
  //   margin-left: 30px;
  gap: 10px;
  margin-top: 20px;
`;

export const TabNavigator = styled.div`
  display: flex;
  width: max-content;
  padding: 5px 5px;
  background-color: #1d1d1d;
  gap: 10px;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
`;

export const Tab = styled.div`
  font-size: 15px;
  font-weight: bold;
  border-radius: 5px;
  cursor: pointer;
  color: #616056;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 20px;
  padding: 10px 15px;

  &:hover,
  &[data-selected="true"] {
    background-color: #f1eed3;
    color: #0f0f0f;
  }
`;

export const YearTab = styled.div``;

export const RemoveTabButton = styled.div`
  cursor: pointer;
  transition: ease-in-out 0.2s;

  &:hover {
    color: #96080a;
  }
`;

export const TabItems = styled.div`
  color: #616056;
  font-size: 20px;
  font-weight: bold;
  //   padding: 10px 15px;
  cursor: pointer;
  display: flex;
  align-items: center;
`;

export const SquiglyArrow = styled.img`
`;

// Mock Tab
export const TabM = styled.div`
  font-size: 15px;
  font-weight: bold;
  border-radius: 5px;
  color: #616056;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 20px;
  padding: 10px 15px;

  &:hover {
    opacity: 0.5;
    background-color: #f1eed3;
    color: #0f0f0f;
  }
`;

export const YearTabM = styled.div``;

export const RemoveTabButtonM = styled.div``;

//The saved course box
export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  //   margin-left: 30px;
  padding-top: 30px;
`;

export const YearIndicator = styled.div`
  width: max-content;
  font-weight: bold;
  font-size: 25px;
  color: #f1eed3;
`;

export const SelectCourse = styled.div`
  padding: 20px 30px;
  background-color: #1d1d1d;
  border-radius: 5px;
  width: max-content;
  color: #f1eed3;
  font-size: 15px;
  font-weight: bold;
  cursor: pointer;
  border: 1px solid #1d1d1d;
  margin-top: 20px;

  &:hover {
    transition: ease-in-out 0.2s;
    border: 1px solid #f1eed3;
    background-color: transparent;
    color: #f1eed3;
  }
`;

export const SelectCourseM = styled.div`
  opacity: 0.5;
  padding: 20px 30px;
  background-color: #1d1d1d;
  border-radius: 5px;
  width: max-content;
  color: #f1eed3;
  font-size: 15px;
  font-weight: bold;
  cursor: not-allowed;
  border: 1px solid #1d1d1d;
  margin-top: 20px;
`;

// The course List Display

export const CourseListing = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 800px;
  overflow-y: auto;
  overflow-x: hidden;
  //   width: max-content;
  margin-right: 50px;
  padding-right: 100px;

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #353430; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const CourseBox = styled.div`
  display: flex;
  gap: 20px;
  align-items: center;
  margin-top: 20px;
`;

export const SubCourseBox = styled.div`
  padding: 20px 10px;
  border: 2px solid #f1eed3;
  border-radius: 10px;
  align-items: center;
  width: max-content;
  display: flex;
`;

export const CrossesImage = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
  opacity: 0;
  z-index: 2;
`;

export const LeftCourseBox = styled.div`
  display: flex;
  // background-color: #f1eed3;
  border-right: 2px solid #616056;
  height: 100%;
  align-items: center;
  justify-content: center;
  gap: 5px;
  padding: 10px 20px;
  width: 100px;
  flex-direction: column;
  //   border-radius: 10px;
`;

export const CoursePrefixContainer = styled.div`
  color: #f1eed3;
  //   color: #0f0f0f;
  font-weight: bold;
`;

export const CourseCodeContainer = styled.div`
  //   color: #0f0f0f;
  color: #f1eed3;
  font-weight: bold;
`;

export const RightCourseBox = styled.div`
  color: #f1eed3;
  gap: 10px;
  display: flex;
  height: 100%;
  align-items: center;
  //   flex-direction: column;
  width: 700px;
`;

export const LeftSub = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 10px 10px;
  border-right: 2px solid #616056;
  font-weight: bold;
  gap: 5px;
`;

export const CourseTermContainer = styled.div``;

export const RigthSub = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 10px;
  justify-content: center;
  gap: 5px;
`;

export const CourseNameContainer = styled.div``;

export const CourseClassificationContainer = styled.div`
  color: #f1eed3;
  opacity: 0.4;
`;

export const RemoveButton = styled.div`
  color: #f1eed3;
  opacity: 0.1;
  font-weight: bold;
  cursor: pointer;
  align-items: center;
  display: flex;
  margin-left: 10px;

  transition: ease-in-out 0.2s;

  &:hover {
    opacity: 0.3;
    color: white;
  }
`;

// The mock up course display
export const CourseBoxM = styled.div`
  display: flex;
  gap: 20px;
  align-items: center;
  margin-top: 20px;
  opacity: 0.5;
  cursor: not-allowed;
`;

export const SubCourseBoxM = styled.div`
  padding: 20px 10px;
  border: 2px solid #f1eed3;
  border-radius: 10px;
  align-items: center;
  width: max-content;
  display: flex;
  opacity: 0.5;
`;

export const LeftCourseBoxM = styled.div`
  display: flex;
  // background-color: #f1eed3;
  border-right: 2px solid #616056;
  height: 100%;
  align-items: center;
  justify-content: center;
  gap: 5px;
  padding: 10px 20px;
  width: 100px;
  flex-direction: column;
  //   border-radius: 10px;
`;

export const CoursePrefixContainerM = styled.div`
  color: #f1eed3;
  //   color: #0f0f0f;
  font-weight: bold;
`;

export const CourseCodeContainerM = styled.div`
  //   color: #0f0f0f;
  color: #f1eed3;
  font-weight: bold;
`;

export const RightCourseBoxM = styled.div`
  color: #f1eed3;
  gap: 10px;
  display: flex;
  height: 100%;
  align-items: center;
  //   flex-direction: column;
  width: 700px;
`;

export const LeftSubM = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 10px 10px;
  border-right: 2px solid #616056;
  font-weight: bold;
  gap: 5px;
`;

export const CourseTermContainerM = styled.div``;

export const RigthSubM = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 10px;
  justify-content: center;
  gap: 5px;
`;

export const CourseNameContainerM = styled.div``;

export const CourseClassificationContainerM = styled.div`
  color: #f1eed3;
  opacity: 0.4;
`;

export const RemoveButtonM = styled.div`
  color: #f1eed3;
  opacity: 0.1;
  font-weight: bold;
  align-items: center;
  display: flex;
  margin-left: 10px;
`;

// Put the popup shit here

// Popup Input
export const InputContainer = styled.div`
  display: flex;
  gap: 20px;
`;

export const Input = styled.input`
  width: 500px;
  border-radius: 25px;
  height: 40px;
  font-size: 20px;
  padding: 10px;
  border: 0px;
  outline: 0px;
  background-color: #b7b4a0;
  color: #0f0f0f;
  font-size: 18px;

  @media screen and (max-width: 550px) {
    width: 250px;
  }
`;

export const EnterButton = styled.div`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;

  &:hover {
    border-radius: 0px;
    border: 2px solid #0f0f0f;
    background-color: #f1eed3;
    color: #0f0f0f;
    cursor: pointer;
  }
`;

// Popup Content
export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const PopupContent = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #1d1d1d;
  border-radius: 25px;
  padding: 20px;
`;

export const CourseList = styled.ul`
  list-style: none;
`;

export const CourseItem = styled.li`
  margin-bottom: 10px;
`;

export const List = styled.ul`
  list-style: none;
  margin-bottom: 25px;
  padding: 0;
  width: 100%;
  overflow-y: auto; // make the list scrollable vertically
  max-height: 300px; // limit the height of the list

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #0f0f0f; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const Item = styled.li`
  margin: 10px;
  padding: 10px;
  border: 2px solid #0f0f0f;
  border-radius: 25px;
  background-color: #0f0f0f;
  color: #f1eed3;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media screen and (max-width: 640px) {
    flex-direction: column;
  }
`;

export const ResultContainer = styled.div``;

export const AddButtonContainer = styled.div`
  margin-right: 10px;
`;

export const AddButton = styled.button`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;

  &:hover {
    border-radius: 0px;
    border: 2px solid #0f0f0f;
    background-color: #f1eed3;
    color: #0f0f0f;
    cursor: pointer;
  }
`;

export const ResultItem = styled.div`
  margin-bottom: 5px;
  padding: 5px 10px;
`;

export const Highlight = styled.span`
  background-color: #3f3f3a;
`;

// The Eligible Medschools Section

export const MedschoolCounter = styled.div`
  display: flex;
  font-size: 14px;
  //   font-weight: bold;
  color: #f1eed3;
  padding-top: 20px;
`;

export const CheckEligebilityContainer = styled.div`
  //   margin-left: 30px;
  margin-top: 30px;
`;

export const MedSchoolScrollBox = styled.div`
  display: flex;
  width: 1430px;
  height: 355px;
  min-height: 0px;
  padding-bottom: 10px;
  overflow-x: scroll;
  margin-top: 20px;

  &::-webkit-scrollbar {
    height: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #f1eed3; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const MedSchoolBox = styled.div`
  display: flex;
  flex-direction: row;
  gap: 20px;
  width: max-content;
  height: 100%;
`;

export const CheckEligibilityButton = styled.div`
  padding: 20px 30px;
  background-color: #1d1d1d;
  border-radius: 5px;
  width: max-content;
  color: #f1eed3;
  font-size: 15px;
  font-weight: bold;
  cursor: pointer;
  border: 1px solid #1d1d1d;
  margin-top: 20px;

  &:hover {
    transition: ease-in-out 0.2s;
    border: 1px solid #f1eed3;
    background-color: transparent;
    color: #f1eed3;
  }
`;

export const CheckEligebilityTitle = styled.div`
  width: max-content;
  font-weight: bold;
  font-size: 25px;
  color: #f1eed3;
`;

export const MedSchools = styled.div`
  height: 250px;
`;

export const SubBox = styled.div`
  padding: 50px 20px;
  background-color: black;
  border-radius: 25px;
  width: 500px;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const BoxTopContainer = styled.div`
  display: flex;
  color: white;
  justify-content: space-between;
  align-items: end;
  padding-left: 20px;
  padding-right: 20px;
`;

export const MedSchoolName = styled.div`
  font-size: 25px;
  font-weight: bold;
  color: #f1eed3;
`;

export const MedSchoolGPA = styled.div`
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const MedSchoolImage = styled.img`
  opacity: 0.2;
  width: auto;
  height: 100%;
  justify-content: center;
`;

export const MedSchoolImageContainer = styled.div`
  width: 100%;
  height: 170px;
  display: flex;
  justify-content: center;
`;

export const BoxBottomContainer = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
  align-items: start;
`;

export const MedSchoolPrequisitesTitle = styled.div`
  display: flex;
  font-size: 20px;
  font-weight: bold;
  color: #616056;
  padding-left: 20px;
  padding-right: 20px;
`;

export const PrereqList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  gap: 20px;
  margin-top: 10px;
  padding-left: 20px;
  padding-right: 20px;
`;

export const ListItems = styled.div`
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const MedSchoolsM = styled.div`
  height: 250px;
  opacity: 0.2;
  cursor: not-allowed;
`;

export const SubBoxM = styled.div`
  padding: 50px 20px;
  background-color: black;
  border-radius: 25px;
  width: 500px;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const BoxTopContainerM = styled.div`
  display: flex;
  color: white;
  justify-content: space-between;
  align-items: end;
  padding-left: 20px;
  padding-right: 20px;
`;

export const MedSchoolNameM = styled.div`
  font-size: 25px;
  font-weight: bold;
  color: #f1eed3;
`;

export const MedSchoolGPAM = styled.div`
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const MedSchoolImageM = styled.img`
  opacity: 0.2;
  width: auto;
  height: 100%;
  justify-content: center;
`;

export const MedSchoolImageContainerM = styled.div`
  width: 100%;
  height: 170px;
  display: flex;
  justify-content: center;
`;

export const BoxBottomContainerM = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
  align-items: start;
`;

export const MedSchoolPrequisitesTitleM = styled.div`
  display: flex;
  font-size: 20px;
  font-weight: bold;
  color: #616056;
  padding-left: 20px;
  padding-right: 20px;
`;

export const PrereqListM = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  gap: 20px;
  margin-top: 10px;
  padding-left: 20px;
  padding-right: 20px;
`;

export const ListItemsM = styled.div`
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;
