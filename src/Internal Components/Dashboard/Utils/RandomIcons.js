import { BigHead } from "@bigheads/core";

const accessories = ["none", "roundGlasses", "tinyGlasses", "shades"];
const bodies = ["breasts", "chest"];
const circleColors = ["blue", "green", "red"];
const clothing = ["dressShirt", "shirt", "vneck", "tankTop", "dress"];
const clothingColors = ["red", "blue", "green"];
const eyebrows = ["serious", "angry", "raised"];
const eyes = [
  "dizzy",
  "happy",
  "content",
  "squint",
  "simple",
  "wink",
  "normal",
  "heart",
];
const faceMaskColors = ["red", "blue", "green"];
const facialHair = ["none3", "stubble"];
const graphics = ["redwood", "vue", "react"];
const hair = [
  "afro",
  "balding",
  "none",
  "long",
  "bun",
  "short",
  "pixie",
  "buzz",
  "bob",
];
const hairColors = ["black", "blonde", "white", "brown"];
const hats = ["none5", "beanie"];
const hatColors = ["white", "black"];
const lipColors = ["green", "purple"];
const mouths = [
  "sad",
  "openSmile",
  "grin",
  "lips",
  "open",
  "serious",
  "tongue",
];
const skinTones = ["yellow", "light", "brown", "dark", "red", "black"];

function getRandomValue(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export const RandIcon = () => (
  <BigHead
    accessory={getRandomValue(accessories)}
    body={getRandomValue(bodies)}
    circleColor={'#0f0f0f'}
    clothing={getRandomValue(clothing)}
    clothingColor={getRandomValue(clothingColors)}
    eyebrows={getRandomValue(eyebrows)}
    eyes={getRandomValue(eyes)}
    faceMask={false}
    faceMaskColor={getRandomValue(faceMaskColors)}
    facialHair={getRandomValue(facialHair)}
    graphic={getRandomValue(graphics)}
    hair={getRandomValue(hair)}
    hairColor={getRandomValue(hairColors)}
    hat={getRandomValue(hats)}
    hatColor={getRandomValue(hatColors)}
    lashes
    lipColor={getRandomValue(lipColors)}
    mask
    mouth={getRandomValue(mouths)}
    skinTone={getRandomValue(skinTones)}
  />
);
