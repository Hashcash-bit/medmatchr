//Important react deps
import React, { useEffect, useState } from "react";

//Importing the context
import { UserAuth } from "../../Context/AuthContext";

//Importing the Link
import { Link, useNavigate } from "react-router-dom";

//Toasts
import { ToastContainer, toast } from "react-toastify";

//Styled Components
import {
  BottomDiv,
  BottomRightBox,
  BottomRightContainer,
  Box,
  Container,
  DateContainer,
  EmailContainer,
  LeftSide,
  RightBox,
  RightSide,
  TopDiv,
  TopRightBox,
  TopRightContainer,
  UserIconContainer,
  UserPicContainer,
  Username,
  UsernameContainer,
} from "./DashboardStyles";

//Importing the Icons
import { RandIcon } from "./Utils/RandomIcons";
import Sidebar from "./Components/Navigation/Sidebar";
import TopInfo from "./Components/TopInfo/TopInfo";
import Schedule from "./Components/Schedule/Schedule";
import TopBar from "../Universal Components/TopBar";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../../External Components/firebase/Firebase";

export default function Dashboard() {
  //Checking to see is the auth context works
  const { user, logout, username } = UserAuth();
  const [courses, setCourses] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const retrieveCourses = async () => {
    if (!user || !user.uid) {
      return;
    }

    console.log("User UID:", user.uid); // Check if user.uid is available

    const coursesRef = collection(db, "users", user.uid, "courses");
    const querySnapshot = await getDocs(coursesRef);
    const courses = querySnapshot.docs.map((doc) => doc.data());

    setCourses(courses);
  };

  useEffect(() => {
    if (user && user.uid) {
      retrieveCourses();
      setIsLoading(false);
    }
  }, [user]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Container>
        <TopBar />
        <Box>
          <ul>
            {courses.map((course) => (
              <li key={course.code}>
                <h2>{course.name}</h2>
                <p>Prefix: {course.prefix}</p>
                <p>Code: {course.code}</p>
                <p>Terms: {course.terms}</p>
                <p>Classification: {course.classification}</p>
              </li>
            ))}
          </ul>
        </Box>
      </Container>
    </>
  );
}
