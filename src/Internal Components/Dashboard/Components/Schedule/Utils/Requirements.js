const medSchools = [
  {
    name: "University of Calgary",
    prerequisites: [
      { classification: "Humanities", terms: 1 },
      { classification: "Biology", terms: 1 },
      { classification: "Biochemistry", terms: 1 },
      { classification: "Genetics", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.8,
  },
  {
    name: "University of Alberta",
    prerequisites: [
      { classification: "Humanities", terms: 1 },
      { classification: "Biology", terms: 1 },
      { classification: "Biochemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.5,
  },
  {
    name: "University of British Columbia",
    prerequisites: [
      { classification: "Humanities", terms: 1 },
      { classification: "Biology", terms: 1 },
      { classification: "Biochemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.8,
  },
  {
    name: "University of Toronto",
    prerequisites: [
      { classification: "Humanities", terms: 1 },
      { classification: "Biochemistry", terms: 2 },
      { classification: "Genetics", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.6,
  },
  {
    name: "McMaster University",
    prerequisites: [
      { classification: "Humanities", terms: 1 },
      { classification: "Biochemistry", terms: 2 },
      { classification: "Genetics", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.0,
  },
  {
    name: "Queen's University",
    prerequisites: [
      { classification: "Humanities", terms: 2 },
      { classification: "Biology", terms: 1 },
      { classification: "Chemistry", terms: 2 },
      // more prerequisites
    ],
    gpaRequirement: 3.0,
  },
  {
    name: "McGill University",
    prerequisites: [
      { classification: "Humanities", terms: 2 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.95,
  },
  {
    name: "Western University",
    prerequisites: [
      { classification: "Humanities", terms: 2 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 2 },
      // more prerequisites
    ],
    gpaRequirement: 3.7,
  },
  {
    name: "University of Ottawa",
    prerequisites: [
      { classification: "Humanities", terms: 1 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.85,
  },
  {
    name: "NewfoundLand/Memorial University",
    prerequisites: [
      { classification: "Humanities", terms: 3 },
      { classification: "Biology", terms: 1 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.7,
  },
  {
    name: "Dalhousie University",
    prerequisites: [
      { classification: "Humanities", terms: 4 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.7,
  },
  {
    name: "University of Saskatchewan",
    prerequisites: [
      { classification: "Humanities", terms: 4 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.8,
  },
  {
    name: "Northern Ontario University",
    prerequisites: [
      { classification: "Humanities", terms: 4 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.0,
  },
  {
    name: "University of Manitoba",
    prerequisites: [
      { classification: "Humanities", terms: 4 },
      { classification: "Biology", terms: 2 },
      { classification: "Chemistry", terms: 1 },
      // more prerequisites
    ],
    gpaRequirement: 3.3,
  },
  // more med schools
  {
    name: "Harvard University",
    prerequisites: [
      //{ classification: "Humanities", terms: 0 },
      //{ classification: "English", terms: 2 },
      //{ classification: "Biology", terms: 2 },
      //{ classification: "Chemistry", terms: 1 },
      // First to check for nonlab courses (Includes both lab and no lab)
      //{ classification: "Biochemistry,Inorganic/Introductory Chemistry,Organic Chemistry,Biochemistry Lab,Inorganic/Introductory Chemistry Lab,Organic Chemistry Lab", terms: 4},
      { classification: "Biochemistry,Inorganic/Introductory Chemistry Lab,Organic Chemistry Lab", terms: 4}

    ],
    gpaRequirement: 3.9,
  },
];

export default medSchools;
