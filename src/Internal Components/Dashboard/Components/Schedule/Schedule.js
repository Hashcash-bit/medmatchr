import React, { useState } from "react";

//Import the courses
import courses from "../../../../External Components/OnBoarding/Components/Utils/Course";

//Importing the Universities
import medSchools from "./Utils/Requirements";

//Importing the toast
import { ToastContainer, toast } from "react-toastify";

// Importing the styled components
import { styled } from "styled-components";
import { EnterButton } from "./ScheduleStyles";
import {
  AddButton,
  AddButtonContainer,
  Input,
  InputContainer,
  Item,
  List,
  ResultContainer,
} from "./ScheduleStyles";
import { Box } from "../../../../External Components/OnBoarding/Components/CourseSearch/CourseSearchStyles";

//The local styled content
const ResultItem = styled.div`
  margin-bottom: 5px;
  padding: 5px 10px;
`;

const Highlight = styled.span`
  background-color: #3f3f3a;
`;

function Schedule() {
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedCourses, setSelectedCourses] = useState([]);
  const [eligibleSchools, setEligibleSchools] = useState([]);
  const [universityFilter, setUniversityFilter] = useState("");
  const [results, setResults] = useState([]);
  const [savedCourses, setSavedCourses] = useState([]);

  // The highlighting logic
  const highlightSearchTerm = (text) => {
    const parts = text.split(new RegExp(`(${searchTerm})`, "gi"));
    return (
      <>
        {parts.map((part, i) =>
          part.toLowerCase() === searchTerm.toLowerCase() ? (
            <Highlight key={i}>{part}</Highlight>
          ) : (
            part
          )
        )}
      </>
    );
  };

  //Saved Courses
  const isCourseSaved = (course) =>
    selectedCourses.some(
      (savedCourse) =>
        savedCourse.university === course.university &&
        savedCourse.prefix === course.prefix &&
        savedCourse.code === course.code
    );

  const handleCourseSelect = (course) => {
    setSelectedCourses((prevCourses) => [...prevCourses, course]);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!searchTerm) {
      toast.error("Please enter a search term");
      return;
    }
    let filteredCourses = courses.filter((course) => {
      if (universityFilter && course.university !== universityFilter) {
        return false;
      }
      return (
        course.university.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.prefix.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.code.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
        course.classification.toLowerCase().includes(searchTerm.toLowerCase())
      );
    });
    setResults(filteredCourses);
  };

  const checkEligibility = () => {
    const eligible = medSchools.filter((school) => {
      return school.prerequisites.every((prereq) => {
        const totalTerms = selectedCourses.reduce((acc, course) => {
          let tempArr = prereq.classification.split(',');
          console.log("Success")
          if (tempArr.length > 1) {
            for (let i = 0; i < tempArr.length; i++) {
              if (course.classification == tempArr[i]) {
                acc += course.terms;
              };
              
            };
            return acc;
          } else {
            if (course.classification === prereq.classification) {
              return acc + course.terms;
            }
          }
          return acc;
        }, 0);
        return totalTerms >= prereq.terms;
      });
    });
    setEligibleSchools(eligible);
  };

  return (
    <div>
      <Box>
        <InputContainer>
          <Input
            type="text"
            placeholder="Enter course code, name or classification..."
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <EnterButton onClick={handleSubmit}>Search</EnterButton>
        </InputContainer>
        {results.length > 0 && (
          <div>
            Number of results: <strong>{results.length}</strong>
          </div>
        )}
        <List>
          {results.map((result, index, course) => (
            <Item key={index}>
              <ResultContainer>
                <ResultItem>
                  <strong>University:</strong>{" "}
                  {highlightSearchTerm(result.university)}
                </ResultItem>
                <ResultItem>
                  <strong>Prefix:</strong> {highlightSearchTerm(result.prefix)}
                </ResultItem>
                <ResultItem>
                  <strong>Code:</strong> {highlightSearchTerm(result.code)}
                </ResultItem>
                <ResultItem>
                  <strong>Name:</strong> {highlightSearchTerm(result.name)}
                </ResultItem>
                <ResultItem>
                  <strong>Classification:</strong>{" "}
                  {highlightSearchTerm(result.classification)}
                </ResultItem>
                <ResultItem>
                  <strong>Lab:</strong> {highlightSearchTerm(result.lab)}
                </ResultItem>
              </ResultContainer>
              <AddButtonContainer>
                {!isCourseSaved(result) && (
                  <AddButton onClick={() => handleCourseSelect(result)}>
                    Add
                  </AddButton>
                )}
              </AddButtonContainer>
            </Item>
          ))}
        </List>
        <h2>Selected Courses:</h2>
        <ul>
          {selectedCourses.map((result) => (
            <li key={result.code}>
              {result.prefix} {result.code}: {result.name}
            </li>
          ))}
        </ul>
        <button onClick={checkEligibility}>Check Eligibility</button>
        {eligibleSchools.map((school) => (
          <div key={school.name}>
            <h2>{school.name}</h2>
            <p>GPA Requirement: {school.gpaRequirement}</p>
            <p>Prerequisite Requirements:</p>
            <ul>
              {school.prerequisites.map((prereq, index) => (
                <li key={index}>
                  {prereq.classification}: {prereq.terms} terms
                </li>
              ))}
            </ul>
          </div>
        ))}
      </Box>
    </div>
  );
}

export default Schedule;
