import { styled } from "styled-components";

export const EnterButton = styled.div`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;

  &:hover {
    border-radius: 0px;
    border: 2px solid #0f0f0f;
    background-color: #f1eed3;
    color: #0f0f0f;
    cursor: pointer;
  }
`;

export const InputContainer = styled.div`
  display: flex;
  gap: 20px;

  @media screen and (max-width: 900px) {
    flex-direction: column;
    justify-content: center;
    align-content: center;
    align-items: center;
  }
`;

export const List = styled.ul`
  list-style: none;
  margin-bottom: 25px;
  padding: 0;
  width: 80%;
  overflow-y: auto; // make the list scrollable vertically
  max-height: 300px; // limit the height of the list

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #0f0f0f; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const Item = styled.li`
  margin: 10px;
  padding: 10px;
  border: 2px solid #0f0f0f;
  border-radius: 25px;
  background-color: #0f0f0f;
  color: #f1eed3;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media screen and (max-width: 640px) {
    flex-direction: column;
  }
`;

export const ResultContainer = styled.div``;

export const AddButtonContainer = styled.div`
  margin-right: 10px;
`;

export const AddButton = styled.button`
  padding: 9px 40px 10px 40px;
  background-color: #0f0f0f;
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  text-decoration: none;
  display: flex;
  border: 2px solid #f1eed3;
  width: max-content;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  border-radius: 25px;
  transition: ease-in-out 0.3s;

  &:hover {
    border-radius: 0px;
    border: 2px solid #0f0f0f;
    background-color: #f1eed3;
    color: #0f0f0f;
    cursor: pointer;
  }
`;

export const Input = styled.input`
  width: 500px;
  border-radius: 25px;
  height: 40px;
  font-size: 20px;
  padding: 10px;
  border: 0px;
  outline: 0px;
  background-color: #b7b4a0;
  color: #0f0f0f;
  font-size: 18px;

  @media screen and (max-width: 550px) {
    width: 250px;
  }
`;
