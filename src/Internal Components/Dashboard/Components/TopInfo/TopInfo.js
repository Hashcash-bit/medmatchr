//Important React deps
import React, { useState } from "react";

//Context
import { UserAuth } from "../../../../Context/AuthContext";

//Styled Components
import {
  BottomRightBox,
  BottomRightContainer,
  DateContainer,
  EmailContainer,
  RightBox,
  TopDiv,
  TopRightBox,
  TopRightContainer,
  UserIconContainer,
  UserPicContainer,
  Username,
  UsernameContainer,
  PencilMove,
} from "../../DashboardStyles";

//Icon(s) import
import { RandIcon } from "../../Utils/RandomIcons";
import { BiPencil } from "react-icons/bi";

//Importing the Username Modal
import UsernameModal from "../../../../Utils/UsernameModal";

export default function TopInfo() {
  //User context
  const { user, username } = UserAuth();

  // The current date
  const currentDate = new Date();
  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const formattedDate = currentDate.toLocaleDateString("en-US", options);

  //Opening the username modal
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <RightBox>
      <TopRightContainer>
        <TopRightBox>
          <TopDiv>
            <UserIconContainer>
              <UserPicContainer>
                <RandIcon />
              </UserPicContainer>
              <UsernameContainer>
                <Username>
                  Hey, <div style={{ fontWeight: "bold" }}>{username}</div>{" "}
                  <PencilMove>
                    <BiPencil onClick={() => setIsModalOpen(true)} />
                  </PencilMove>
                </Username>
                <EmailContainer>
                  You are signed under this email <strong>{user.email}</strong>
                </EmailContainer>
              </UsernameContainer>
            </UserIconContainer>
            <DateContainer>{formattedDate}</DateContainer>
          </TopDiv>
        </TopRightBox>
      </TopRightContainer>
      <BottomRightContainer>
        <BottomRightBox></BottomRightBox>
      </BottomRightContainer>
      <UsernameModal
        isOpen={isModalOpen}
        onClose={() => setIsModalOpen(false)}
      />
    </RightBox>
  );
}
