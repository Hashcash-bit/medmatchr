//React deps
import React from "react";

//Styled Components
import {
  BottomLeftContainer,
  ButtonContainer,
  ButtonText,
  IconContainer,
  LeftBox,
  LinkContainer,
  LinkText,
  LogoName,
  MiddleLeftContainer,
  SideLinks,
  TopLeftContainer,
} from "../../DashboardStyles";

//Auth Context
import { UserAuth } from "../../../../Context/AuthContext";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { BiLogOut, BiSolidDashboard, BiSolidUser } from "react-icons/bi";
import { BsFillCalendarWeekFill } from "react-icons/bs";
import { FiHelpCircle } from "react-icons/fi";

export default function Sidebar() {
  const { logout, username } = UserAuth();
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await logout();
      toast.success("You have been logged out");
      navigate("/");
      console.log("You are logged out");
    } catch (e) {
      console.log(e.message);
      toast.error("Error logging out");
    }
  };
  return (
    <LeftBox>
      <TopLeftContainer>
        <LogoName>MEDMATCHR</LogoName>
      </TopLeftContainer>
      <MiddleLeftContainer>
        <SideLinks>
          <LinkText>
            <IconContainer>
              <BiSolidDashboard style={{ fontSize: "20px" }} />
            </IconContainer>
            <LinkContainer>Dashboard</LinkContainer>
          </LinkText>
          <LinkText>
            <IconContainer>
              <BsFillCalendarWeekFill style={{ fontSize: "20px" }} />
            </IconContainer>
            <LinkContainer to={`/Schedule/${username}`}>Schedule</LinkContainer>
          </LinkText>
          <LinkText>
            <IconContainer>
              <BiSolidUser style={{ fontSize: "20px" }} />
            </IconContainer>
            <LinkContainer>Profile</LinkContainer>
          </LinkText>
        </SideLinks>
      </MiddleLeftContainer>
      <BottomLeftContainer>
        <ButtonContainer>
          <ButtonText>
            <IconContainer>
              <FiHelpCircle style={{ fontSize: "20px" }} />
            </IconContainer>
            <LinkContainer>Help & Information</LinkContainer>
          </ButtonText>
          <ButtonText>
            <IconContainer>
              <BiLogOut style={{ fontSize: "20px" }} />
            </IconContainer>
            <LinkContainer onClick={handleLogout}>Logout</LinkContainer>
          </ButtonText>
        </ButtonContainer>
      </BottomLeftContainer>
    </LeftBox>
  );
}
