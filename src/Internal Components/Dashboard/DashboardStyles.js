//Importing styled components
import { Link } from "react-router-dom";
import styled from "styled-components";

export const Container = styled.div`
  background-color: #0f0f0f;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px 30px;
  font-family: Montserrat-Alt1;
  cursor: default;
`;

export const Box = styled.div`
  width: 100%;
  height: max-content;
  display: flex;
`;

export const LeftSide = styled.div`
  width: 350px;
  height: 100%;
  display: flex;
  justify-content: center;
  //   margin-left: 25px;
  align-items: center;
`;

export const LeftBox = styled.div`
  background-color: #f1eed3;
  height: 774px;
  width: 300px;
  border-radius: 25px;
  display: flex;
  flex-direction: column;
  justify-content: start;
`;

export const TopLeftContainer = styled.div`
  display: flex;
  margin-left: 25px;
  margin-top: 35px;
`;

export const LogoName = styled.div`
  font-weight: bold;
  font-size: 20px;
  color: #0f0f0f;
  align-items: center;
  align-content: center;
  align-text: center;
  display: flex;
  cursor: default;
`;

export const MiddleLeftContainer = styled.div`
  height: 300px;
  display: flex;
  margin-top: 90px;
`;

export const SideLinks = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 25px;
  gap: 30px;
`;

export const IconContainer = styled.div``;

export const LinkContainer = styled(Link)`
  text-decoration: none;
  transition: ease-in-out 0.2s;
  color: gray;
  font-size: 14px;
  font-weight: 500;

  &:hover {
    font-weight: bold;
    // font-size: 20px;
    color: #0f0f0f;
  }
`;

export const LinkText = styled.div`
  text-align: center;
  transition: transform 0.3s, box-shadow 0.3s;
  align-items: center;
  display: flex;
  color: gray;
  justify-content: start;
  gap: 15px;
`;

export const BottomLeftContainer = styled.div`
  margin-top: 230px;
`;

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 25px;
  gap: 30px;
`;

export const ButtonText = styled.div`
  text-decoration: none;
  text-align: center;
  transition: transform 0.3s, box-shadow 0.3s;
  align-items: center;
  display: flex;
  color: gray;
  justify-content: start;
  gap: 15px;
`;

export const RightSide = styled.div`
  width: 1140px;
  height: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const RightBox = styled.div`
  width: 97.7%;
  height: 93.7%;
  border-radius: 25px;
`;

export const TopRightContainer = styled.div`
  height: max-content;
`;

export const TopRightBox = styled.div`
  border-radius: 25px;
  height: max-content;
  background-color: #f1eed3;
`;

export const TopDiv = styled.div`
  border-radius: 25px;
  height: max-content;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const UserIconContainer = styled.div`
  display: flex;
  gap: 20px;
  align-items: center;
`;

export const UserPicContainer = styled.div`
  width: 94px;
  height: auto;
  border-radius: 50px;
  background-color: transparent;
  overflow: hidden;
  align-items: center;
  display: flex;
  justify-content: center;
  margin-top: -15px;
  padding-left: 3px;
  padding-right: 3px;
`;

export const UsernameContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 5px;
`;

export const EmailContainer = styled.div``;

export const Username = styled.div`
  font-size: 22px;
  gap: 5px;
  display: flex;
  align-items: center;
`;

export const PencilMove = styled.div`
  transition: ease-in-out 0.3s;
  &:hover {
    translate: 5px;
    cursor: pointer;
  }
`;

export const DateContainer = styled.div`
  margin-right: 25px;
  padding: 10px 40px;
  background-color: #b7b4a0;
  border-radius: 25px;
  font-weight: bold;
  align-items: center;
  text-align: center;
  display: flex;
  justify-content: center;
`;

export const BottomRightContainer = styled.div``;

export const BottomRightBox = styled.div``;

export const BottomDiv = styled.div``;
