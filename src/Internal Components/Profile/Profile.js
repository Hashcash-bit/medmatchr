import React, { useRef, useState, useEffect } from "react";

// Top Bar
import TopBar from "../Universal Components/TopBar";

// The styled components
import {
  BottomBRBB,
  BottomBRBBInputNew,
  BottomBRBBInputOld,
  BottomBRBBSubTitle,
  BottomBRBBTitle,
  BottomBox,
  BottomRBB,
  BottomRBBInput,
  BottomRBBInputNew,
  BottomRBBInputOld,
  BottomRBBSubTitle,
  BottomRBBTitle,
  Box,
  Container,
  ImageBox,
  ImageDesc,
  ImageInput,
  ImageTitle,
  LeftBottomBox,
  MiddleRBB,
  MiddleRBBEmailContainer,
  MiddleRBBInput,
  MiddleRBBInputContainer,
  MiddleRBBMedschool,
  MiddleRBBSubTitle,
  MiddleRBBTitle,
  MockImage,
  PFP,
  RightBottomBox,
  RightBottomBoxTitle,
  SaveButton,
  SubBox,
  SubTitle,
  SubmitButton,
  Title,
  TopBox,
  TopRBB,
  TopRBBInput,
  TopRBBSubTitle,
  TopRBBTitle,
} from "./ProfileStyles";
import styled from "styled-components";

// React icons
import { BiImageAdd } from "react-icons/bi";

// Importing the auth context
import { UserAuth } from "../../Context/AuthContext";

// Importing the firebase stuff
import { db, storage } from "../../External Components/firebase/Firebase";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { updateDoc, doc } from "firebase/firestore";
import courses from "../../External Components/OnBoarding/Components/Utils/Course";

//Local Styled Components
const FileInputContainer = styled.div``;

const FileInput = styled.input`
  display: none;
`;

const FileInputLabel = styled.label`
  /* Add your custom styles for the label here */
  display: inline-block;
  //   padding: 8px 12px;
  cursor: pointer;
  background-color: transparent;
  color: #f1eed3;
  opacity: 0.5;
  font-weight: bold;
  font-size: 14px;
  transition: 0.2s ease-in-out;

  &:hover {
    opacity: 1;
  }
`;

const Select = styled.select`
  outline: none;
  background-color: #0f0f0f;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

function Profile() {
  // Auth context
  const {
    user,
    username,
    profileImage,
    updateProfileImage,
    updateName,
    firstName: firstNameFromContext,
    lastName: lastNameFromContext,
    desiredMedSchool: desiredMedSchoolFromContext,
    updateMedschool,
    // updatePassword,
  } = UserAuth();
  // State for the image
  const [selectedFile, setSelectedFile] = useState(null);
  // fileInputRef
  const fileInputRef = useRef(null);
  // State for the new username
  const [newUsername, setNewUsername] = useState("");
  // State for the first name
  const [firstName, setFirstName] = useState("");
  // State for the last name
  const [lastName, setLastName] = useState("");
  // State for the university filter
  const [universityFilter, setUniversityFilter] = useState("");
  // State for the desired med school
  const [desiredMedSchool, setDesiredMedSchool] = useState("");
  //State for the old and new password
  // const [oldPassword, setOldPassword] = useState("");
  // const [newPassword, setNewPassword] = useState("");

  const universities = [...new Set(courses.map((course) => course.university))];

  // This handles the file explorer opening
  const handleFileChange = (e) => {
    if (e.target.files[0]) {
      setSelectedFile(e.target.files[0]);
      console.log("Selected file:", e.target.files[0]);
    }
  };

  // Handle the upload to the firestore database
  const handleUpload = async () => {
    console.log("handleUpload called");
    if (selectedFile) {
      const storageRef = ref(storage, `profileImages/${user.uid}`);
      await uploadBytes(storageRef, selectedFile);
      const url = await getDownloadURL(storageRef);
      updateProfileImage(url);
      const userDocRef = doc(db, "users", user.uid);
      await updateDoc(userDocRef, { profileImage: url });
      setSelectedFile(null);
      fileInputRef.current.value = "";
    }
  };

  // This will handle the information change
  const handleUsernameChange = async (event) => {
    event.preventDefault();
    console.log("handleSubmit called");
    try {
      const userRef = doc(db, "users", user.uid);
      await updateDoc(userRef, {
        username: newUsername,
      });
      window.location.reload();
    } catch (error) {
      console.error(error);
    }
  };

  //This will be the handle Name change
  const handleNameChange = async (event) => {
    event.preventDefault();
    console.log("handleNameChange called");
    try {
      await updateName(firstName, lastName);
      console.log("Firestore document updated");
      setFirstName("");
      setLastName("");
    } catch (error) {
      console.error(error);
    }
  };

  //   useEffect(() => {
  //     setFirstName(firstNameFromContext);
  //     setLastName(lastNameFromContext);
  //   }, [firstNameFromContext, lastNameFromContext]);

  // This is the desired uni save's function
  const handleDesiredMedSchoolChange = async (event) => {
    event.preventDefault();
    console.log("handleDesiredMedSchoolChange called");
    try {
      await updateMedschool(desiredMedSchool);
      console.log("Firestore document updated");
      setDesiredMedSchool("");
    } catch (error) {
      console.error(error);
    }
  };

  //This is the function that handles the Password change
  // const handleUpdatePassword = async () => {
  //   await updatePassword(oldPassword, newPassword);
  // };

  return (
    <Container>
      <TopBar />
      <Box>
        <SubBox>
          <TopBox>
            <Title>Settings</Title>
            <SubTitle>Your life, your preferences.</SubTitle>
          </TopBox>
          <BottomBox>
            <LeftBottomBox>
              <ImageTitle>Change your Image here</ImageTitle>
              <ImageBox>
                <MockImage>
                  {profileImage ? (
                    <PFP src={profileImage} alt="Profile" />
                  ) : (
                    <BiImageAdd />
                  )}
                </MockImage>
              </ImageBox>
              <FileInputContainer>
                <FileInput
                  id="file-input"
                  type="file"
                  onChange={handleFileChange}
                  ref={fileInputRef}
                />
                <FileInputLabel htmlFor="file-input">
                  Choose File
                </FileInputLabel>
              </FileInputContainer>
              <ImageDesc>
                Place your profile image here and it will be updated. The image
                size should be less than 1 mb. Your image will be cropped
                automatically.
              </ImageDesc>
              <SubmitButton onClick={handleUpload}>Change</SubmitButton>
            </LeftBottomBox>
            <RightBottomBox>
              {/* <form onSubmit={handleSubmit}> */}
              <RightBottomBoxTitle>General Details</RightBottomBoxTitle>
              <TopRBB>
                <TopRBBTitle>Change your Name</TopRBBTitle>
                <TopRBBSubTitle>
                  Change your name here and it will be updated.
                </TopRBBSubTitle>
                <TopRBBInput
                  type="text"
                  value={newUsername}
                  placeholder={username}
                  onChange={(event) => {
                    setNewUsername(event.target.value);
                    console.log("New username:", event.target.value);
                  }}
                />
                <SaveButton type="submit" onClick={handleUsernameChange}>
                  Update
                </SaveButton>
              </TopRBB>
              <MiddleRBB>
                <MiddleRBBTitle>Personal Information</MiddleRBBTitle>
                <MiddleRBBSubTitle>
                  Enter your personal details here so that we can help you
                  better.
                </MiddleRBBSubTitle>
                <MiddleRBBInputContainer>
                  <MiddleRBBInput
                    placeholder={
                      firstNameFromContext || "Enter your First Name"
                    }
                    value={firstName}
                    onChange={(event) => setFirstName(event.target.value)}
                  />
                  <MiddleRBBInput
                    placeholder={lastNameFromContext || "Enter your Last Name"}
                    value={lastName}
                    onChange={(event) => setLastName(event.target.value)}
                  />
                </MiddleRBBInputContainer>
                {/* <MiddleRBBEmailContainer>
                  <MiddleRBBSubTitle>
                    Change your email here and it will be updated.
                  </MiddleRBBSubTitle>
                  <MiddleRBBInput placeholder={user.email} />
                </MiddleRBBEmailContainer> */}
                <SaveButton type="submit" onClick={handleNameChange}>
                  Update
                </SaveButton>
              </MiddleRBB>
              <BottomRBB>
                <BottomRBBTitle>Update your University</BottomRBBTitle>
                <BottomRBBSubTitle>
                  Enter your University detals here and experience an easier
                  usage of the site.
                </BottomRBBSubTitle>
                <MiddleRBBInputContainer>
                  {/* This is where the university dropdown will go */}
                  <Select
                    value={universityFilter}
                    onChange={(e) => setUniversityFilter(e.target.value)}
                  >
                    <option value="">Select your universities</option>
                    {universities.map((university) => (
                      <option key={university} value={university}>
                        {university}
                      </option>
                    ))}
                  </Select>
                  {/* <BottomRBBInputOld placeholder="Enter your University here" /> */}
                  <BottomRBBInputNew
                    placeholder={
                      desiredMedSchoolFromContext ||
                      "Enter your Desired Medical School"
                    }
                    value={desiredMedSchool}
                    onChange={(event) =>
                      setDesiredMedSchool(event.target.value)
                    }
                  />
                </MiddleRBBInputContainer>
                <SaveButton
                  type="submit"
                  onClick={handleDesiredMedSchoolChange}
                >
                  Update
                </SaveButton>
              </BottomRBB>
              {/* </form> */}
            </RightBottomBox>
          </BottomBox>
        </SubBox>
        {/* <BottomBRBB>
          <BottomBRBBTitle>Change your Password</BottomBRBBTitle>
          <BottomBRBBSubTitle>
            Change your password here and it will be updated.
          </BottomBRBBSubTitle>
          <MiddleRBBInputContainer>
            <BottomBRBBInputOld
              value={oldPassword}
              onChange={(e) => setOldPassword(e.target.value)}
              placeholder="Enter your old password"
            />
            <BottomBRBBInputNew
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
              placeholder="Enter your new password"
            />
          </MiddleRBBInputContainer>
          <SaveButton type="submit" onClick={handleUpdatePassword}>
            Update
          </SaveButton>
        </BottomBRBB> */}
      </Box>
    </Container>
  );
}

export default Profile;
