//Importing the styled
import styled from "styled-components";

export const Container = styled.div`
  background-color: #0f0f0f;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px 30px;
  font-family: Montserrat-Alt1;
  cursor: default;
`;

export const Box = styled.div`
  max-width: 2000px;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  display: flex;
  flex-direction: column;
  // padding-top: 15px;

  &::-webkit-scrollbar {
    width: 5px;
    border-radius: 100px;
    justify-content: none;
    align-items: none;
    align-content: none;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #141413; /* Change the color as desired */
    border-radius: 4px; /* Adjust the border radius as desired */
  }
`;

export const SubBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: start;
  align-items: start;
  gap: 20px;
`;

export const TopBox = styled.div`
  padding: 10px 0px;
  display: flex;
  flex-direction: column;
  gap: 10px;
  width: 100%;
  border-bottom: 2px solid #f1eed3;
`;

export const Title = styled.div`
  color: #f1eed3;
  font-size: 30px;
  font-weight: bold;
`;

export const SubTitle = styled.div`
  color: #f1eed3;
  opacity: 0.5;
  font-size: 15px;
  font-weight: bold;
`;

export const BottomBox = styled.div`
  border-radius: 25px;
  display: flex;
  justify-content: space-between;
  width: 100%;
  
`;

export const LeftBottomBox = styled.div`
  display: flex;
  border: 2px solid #f1eed3;
  border-radius: 25px;
  flex-direction: column;
  justify-content: center;
  padding: 50px 0px;
  height: 528px;
  width: max-content;
  align-items: center;
  gap: 40px;
`;

export const ImageTitle = styled.div`
  color: #f1eed3;
  font-size: 18px;
  font-weight: bold;
  padding: 10px 10px;
`;

export const ImageBox = styled.div`
  display: flex;
  padding: 30px;
  width: max-content;
  height: min-content;
  border-radius: 25px;
  background-color: #222222;
  justify-content: center;
  align-items: center;
`;

export const MockImage = styled.div`
  display: flex;
  color: #f1eed3;
  font-size: 50px;
  overflow: hidden;
  height: 100px;
`;

export const PFP = styled.img`
  height: 100px;
  width: auto;
`;

export const ImageInput = styled.input`
  padding: 10px;
  color: #f1eed3;
  background-color: gray;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ImageDesc = styled.div`
  display: flex;
  color: #f1eed3;
  font-size: 15px;
  font-weight: 500;
  width: 350px;
  padding: 0px 30px;
  line-height: 20px;
  text-align: center;
`;

export const SubmitButton = styled.div`
  padding-top: 15px;
  padding-bottom: 15px;
  padding-right: 40px;
  padding-left: 40px;
  background-color: #f1eed3;
  color: #0f0f0f;
  font-size: 15px;
  border-radius: 25px;
  font-weight: bold;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: 2px solid #f1eed3;
  width: max-content;
  transition: ease-in-out 0.3s;
  cursor: pointer;

  &:hover {
    background-color: transparent;
    color: #f1eed3;
    border-radius: 15px;
  }
`;

export const RightBottomBox = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 50px;
  width: 100%;
`;

export const RightBottomBoxTitle = styled.div`
  color: #f1eed3;
  font-size: 20px;
  font-weight: bold;
`;

export const TopRBB = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 20px 0px;
  margin-top: 13px;
`;

export const TopRBBTitle = styled.div`
  display: flex;
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const TopRBBSubTitle = styled.div`
  display: flex;
  font-size: 12px;
  font-weight: 500;
  color: #f1eed3;
  opacity: 0.5;
`;

export const TopRBBInput = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 100%;
`;

export const MiddleRBB = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 20px 0px;
`;

export const MiddleRBBTitle = styled.div`
  display: flex;
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const MiddleRBBSubTitle = styled.div`
  display: flex;
  font-size: 12px;
  font-weight: 500;
  color: #f1eed3;
  opacity: 0.5;
`;

export const MiddleRBBInputContainer = styled.div`
  display: flex;
  gap: 20px;
  justify-content: space-between;
  width: 100%;
`;

export const MiddleRBBEmailContainer = styled.div`
  display: flex;
  gap: 10px;
  flex-direction: column;
  margin-top: 10px;
`;

export const MiddleRBBInput = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

export const MiddleRBBMedschool = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

export const BottomRBB = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 20px 0px;
`;

export const BottomRBBTitle = styled.div`
  display: flex;
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const BottomRBBSubTitle = styled.div`
  display: flex;
  font-size: 12px;
  font-weight: 500;
  color: #f1eed3;
  opacity: 0.5;
`;

export const BottomRBBInputOld = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

export const BottomRBBInputNew = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

export const BottomBRBB = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 20px 0px;
`;

export const BottomBRBBTitle = styled.div`
  display: flex;
  font-size: 15px;
  font-weight: bold;
  color: #f1eed3;
`;

export const BottomBRBBSubTitle = styled.div`
  display: flex;
  font-size: 12px;
  font-weight: 500;
  color: #f1eed3;
  opacity: 0.5;
`;

export const BottomBRBBInputOld = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

export const BottomBRBBInputNew = styled.input`
  outline: none;
  background-color: transparent;
  border: 2px solid #f1eed3;
  border-radius: 15px;
  font-size: 15px;
  padding: 15px 10px;
  color: #f1eed3;
  width: 50%;
`;

export const SaveButton = styled.div`
  padding-top: 15px;
  padding-bottom: 15px;
  padding-right: 40px;
  padding-left: 40px;
  font-size: 15px;
  font-weight: bold;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: 2px solid #f1eed3;
  width: max-content;
  transition: ease-in-out 0.3s;
  cursor: pointer;
  background-color: transparent;
  color: #f1eed3;
  border-radius: 0px;
  border-radius: 15px;
  margin-top: 10px;
`;
