import { useState } from "react";
import styled from "styled-components";

const FileInput = styled.input`
  display: none;
`;

const FileInputLabel = styled.label`
  /* Add your custom styles for the label here */
  display: inline-block;
  //   padding: 8px 12px;
  cursor: pointer;
  background-color: transparent;
  color: #f1eed3;
  opacity: 0.5;
  font-weight: bold;
  font-size: 14px;
  transition: 0.2s ease-in-out;

  &:hover {
    opacity: 1;
  }
`;

function CustomFileInput() {
  const [selectedFile, setSelectedFile] = useState(null);

  const handleFileChange = (e) => {
    if (e.target.files[0]) {
      setSelectedFile(e.target.files[0]);
      console.log("Selected file:", e.target.files[0]);
    }
  };

  return (
    <>
      <FileInput type="file" id="file-input" onChange={handleFileChange} />
      <FileInputLabel htmlFor="file-input">Choose File</FileInputLabel>
    </>
  );
}

export default CustomFileInput;
