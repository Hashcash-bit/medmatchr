// Default react imports
import React, { useState } from "react";

//The Dropdown
import Dropdown from "../Schedule/Utils/Dropdown";

// Auth Context
import { UserAuth } from "../../Context/AuthContext";

//Styled Components Import
import {
  Box,
  Container,
  LeftContainer,
  LogoContainer,
  LogoName,
  LogoR,
  NavContainer,
  NavItems,
  ProfileContainer,
  ProfileName,
  ProfilePicture,
  RightContainer,
  UniversitySelector,
  DropdownList,
  DropdownItem,
} from "./TopBarStyles";

// Importing the Logo
import RLogo from "../Img/RLogo.svg";

// Importing the random icon
import { RandIcon } from "../Dashboard/Utils/RandomIcons";

// This is where the Routing import will be done
import { useNavigate } from "react-router-dom";

// Importing the toast
import { toast, ToastContainer } from "react-toastify";

export default function TopBar() {
  //The username and logout function
  const { username, logout } = UserAuth();

  // Importing the state for the dropdown
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  // Adding the dropdown logic to the profile container
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await logout();
      toast.success("You have been logged out");
      navigate("/");
      console.log("You are logged out");
    } catch (e) {
      console.log(e.message);
      toast.error("Error logging out");
    }
  };

  return (
    <>
      <ToastContainer />
      <Container>
        <Box>
          <LeftContainer>
            <LogoContainer>
              <LogoR src={RLogo} />
              <LogoName>MEDMATCHR</LogoName>
            </LogoContainer>
            <NavContainer>
              <NavItems to={`/Dashboard/${username}`}>Dashboard</NavItems>
              <NavItems to={`/Schedule/${username}`}>Schedule</NavItems>
              <NavItems to={`/HelpandInformation/${username}`}>
                Help & Information
              </NavItems>
            </NavContainer>
          </LeftContainer>
          <RightContainer>
            <UniversitySelector>
              <Dropdown />
            </UniversitySelector>
            <ProfileContainer onClick={toggleDropdown}>
              <ProfilePicture>
                <RandIcon />
              </ProfilePicture>
              <ProfileName>{username}</ProfileName>
            </ProfileContainer>
            <DropdownList open={dropdownOpen}>
              <DropdownItem to={`/Profile/${username}`}>Settings</DropdownItem>
              <DropdownItem onClick={handleLogout}>Logout</DropdownItem>
            </DropdownList>
          </RightContainer>
        </Box>
      </Container>
    </>
  );
}
