// Styled import
import styled from "styled-components";

//Link Import
import { Link } from "react-router-dom";

export const Container = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: center;
  padding: 10px 0px;
  font-family: Montserrat-Alt1;
  align-items: center;
`;

export const Box = styled.div`
  width: 100%;
  max-width: 2000px;
  display: flex;
  justify-content: space-between;
  padding: 10px 0px;
  align-items: center;
`;

export const LeftContainer = styled.div`
  display: flex;
  height: auto;
  align-items: center;
  gap: 30px;
  // margin-left: 10px;
`;

export const LogoContainer = styled.div`
  display: flex;
  gap: 10px;
  align-items: center;
  //   background-color: red;
`;

export const LogoR = styled.img`
  font-size: 20px;
  font-weight: bold;
  color: #94b2af;
  display: flex;
  height: auto;
  width: auto;
  //   overflow: visible;
`;

export const LogoName = styled.div`
  color: #f1eed3;
  font-weight: bold;
  font-size: 15px;
  //   height: 100%;
  //   align-items: center;
  //   display: flex;
`;

export const NavContainer = styled.div`
  display: flex;
  padding: 10px 10px;
  width: max-content;
  gap: 10px;
  background-color: #f1eed3;
  border-radius: 25px;
  align-items: center;
  justify-content: center;
`;

export const NavItems = styled(Link)`
  font-size: 15px;
  height: min-content;
  width: fit-content;
  font-weight: 600;
  color: #595959;
  cursor: pointer;
  text-decoration: none;
  transition: ease-in-out 0.2s;
  height: 100%;
  padding-top: 2.5px;
  padding-bottom: 2.5px;
  padding-left: 5px;
  padding-right: 5px;
  justify-content: center;
  align-items: center;

  &:hover {
    color: #0f0f0f;
  }
`;

export const RightContainer = styled.div`
  display: flex;
  gap: 10px;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: max-content;
  // margin-right: 10px;
  position: relative;
`;

export const UniversitySelector = styled.div``;

export const ProfileContainer = styled.div`
  background-color: #1d1d1d;
  cursor: pointer;
  width: max-content;
  color: #f1eed3;

  display: flex;
  padding: 4px 10px;
  gap: 10px;
  border-radius: 25px;
  align-items: center;
  justify-content: space-around;
`;

export const ProfilePicture = styled.div`
  height: 30px;
  width: 35px;
  background-color: #0f0f0f;
  padding-bottom: 5px;
  border-radius: 100%;
  overflow: hidden;
  align-items: center;
  display: flex;
  justify-content: center;
  font-size: 100px;
`;

export const ProfileName = styled.div`
  font-size: 15px;
  height: min-content;
  width: max-content;
  font-weight: 600;
  color: #f1eed3;
  cursor: pointer;
  transition: ease-in-out 0.2s;
  height: 100%;
  padding-top: 2.5px;
  padding-bottom: 2.5px;
  padding-left: 5px;
  padding-right: 5px;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

export const DropdownContainer = styled.div`
  position: relative;
`;

export const DropdownList = styled.ul`
  position: absolute;
  top: 100%;
  right: 0;
  background-color: #0f0f0f;
  border: 1px solid #f1eed3;
  border-radius: 10px;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
  list-style-type: none;
  padding: 0;
  margin: 0;
  overflow: hidden;
  display: ${({ open }) => (open ? "flex" : "none")};
  flex-direction: column;
`;

export const DropdownItem = styled(Link)`
  padding: 10px;
  cursor: pointer;
  color: #f1eed3;
  font-size: 15px;
  text-decoration: none;

  &:hover {
    background-color: #222222;
  }
`;
